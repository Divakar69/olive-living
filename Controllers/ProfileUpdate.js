const router = require('express').Router();
const User = require('../Model/User_master');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const axios = require('axios');
const Helper = require('../Helper/Helper');
// const verify = require('./verifyToken');
const Joi = require('joi');



module.exports = {
    ProfileUpdate: async function (req, res) {
        // const schema = Joi.object({
        //     data: Joi.object().required(),
        // });
        const schema = Joi.object({
            name: Joi.string().optional(),
            mobile: Joi.number().min(2).max(13).optional(),
            email: Joi.string().optional(),
            password: Joi.string().regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/).optional(),
            DOB: Joi.date().optional(),
            profession: Joi.string().optional(),
            hobbies: Joi.string().optional(),
            passionate_about: Joi.string().optional(),
            sports: Joi.string().optional()
        });
        const validationresult = schema.validate(req.body.data);
        let response = {};
        if (validationresult.error) {
            let message = validationresult.error.details[0]['message'];
            response = {
                'message': message,
                'status_code': 400
            }
            res.status(400).json(response)
        } else {
            var data = req.body;
            // console.log(data.data.mobile);process.exit()
            if (data.data.mobile) {
                cryptedmobile = data.data.mobile;//Helper.EnCrypt(data.data.mobile)
                data.data.mobile = cryptedmobile;
            }
            if (data.data.email) {
                cryptedemail = data.data.email;//Helper.EnCrypt(data.data.email)
                data.data.email = cryptedemail;
            }
            if (data.data.password) {
                cryptedpassword = data.data.password//Helper.EnCrypt(data.data.password)
                data.data.password = cryptedpassword;
            }
            var where = { _id: req.headers['userid'] }
            var newvalues = { $set: req.body.data }
            const updatePost = await User.updateOne(where, newvalues)
            if (!updatePost) {
                res.status(400).json({
                    status_code: 400,
                    message: err
                })
            }
            else {
                res.status(200).json({
                    status_code: 200,
                    message: "User details update success"
                })
            }

        }
    }
}
