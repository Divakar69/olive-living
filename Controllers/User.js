const Joi = require('joi');
const UserModel = require('../Model/User_master');
const Helper = require('../Helper/Helper')
const axios = require('axios');
const bcrypt = require('bcryptjs');
const user_settings = require('../Model/user_settings');

module.exports = {
    Login: async (req, res) => {
        const Schema = Joi.object({
            mobile: Joi.string().length(10).pattern(/^[0-9]+$/).required()
        })
        const validationresult = Schema.validate(req.body);
        var response = {};
        if (validationresult.error) {
            response = {
                status_code: 400,
                message: validationresult.error.details[0]['message']
            }
            //console.log(response);
            res.status(400).json(response)
        }
        else {
            const mobile = req.body.mobile;
            // params = {
            //     mobile: mobile,
            //     otp: 1234
            // }
            // var is_sent = await Helper.SMS(params);
            // console.log(is_sent);process.exit();
            var otp = await Helper.GetOTP()
            var response1= {};
                var config = {
                    method: 'get',
                    url: 'http://olive.theretailinsights.co/api/v1/vendor/56',
                    headers: { }
                  };
                   await axios(config)
                  .then(function (result) {
                      response1 = {
                          data : result.data
                      }
                    //console.log(JSON.stringify(response.data));
                  })
                  .catch(function (error) {
                    console.log('else')
                    console.log(error);
                  });
            await axios.post(response1.data.data.vendor.vendor_url, {
                '@VER': '1.2',
                USER: {
                    '@USERNAME': response1.data.data.vendor.username,
                    '@PASSWORD': response1.data.data.vendor.password,
                    '@UNIXTIMESTAMP': ""
                },
                DLR: { '@URL': "" },
                SMS: [{
                    '@UDH': "0",
                    '@CODING': "1",

                    '@TEXT': otp + " is the One  Time Password(OTP) for your login to Oliveliving. Please do not share with anyone.", "@PROPERTY": "0", "@ID": "1",
                    ADDRESS: [{
                        '@FROM':response1.data.data.vendor.Address,
                        '@TO': "916303732109",
                        '@SEQ': "1",
                        '@TAG': ""
                    },
                    ]
                }]
            })
                .then(async (ApiResponse) => {
                    // const encryptmobile = await Helper.EnCrypt(mobile);
                    // console.log(encryptmobile)
                    const is_regitered = await UserModel.find({ mobile: mobile }, {})
                    // var cryptedmobile = await Helper.EnCrypt(mobile)
                    // console.log(is_regitered)
                    if (is_regitered.length > 0) {
                        await UserModel.updateOne({ mobile: mobile }, {
                            $set: {
                                otp: otp
                            }
                        })
                        response = {
                            status_code: 200,
                            message: "Otp sent to " + mobile,
                            is_regitered: true
                        }
                        res.status(200).json(response)
                    }
                    else {
                        await new UserModel({
                            mobile: mobile,
                            otp: otp
                        }).save()
                        response = {
                            status_code: 200,
                            message: "Otp sent to " + mobile,
                            is_regitered: false
                        }
                        res.status(200).json(response)
                    }                    //console.log(response);
                })
                .catch(async (error) => {
                    // var response = {
                    //     'status_code': 200,
                    //     'message': error
                    // }
                    // res.status(400).json(response)
                    const is_regitered = await UserModel.find({ mobile: mobile }, {})
                    // var cryptedmobile = await Helper.EnCrypt(mobile)
                    // var cryptedmobile2 = await Helper.EnCrypt(mobile)

                    // console.log(cryptedmobile,  "-----" + cryptedmobile2)
                    if (is_regitered.length > 0) {
                        await UserModel.updateOne({ mobile: mobile }, {
                            $set: {
                                otp: otp
                            }
                        })
                        response = {
                            status_code: 200,
                            message: "Otp sent to " + mobile,
                            is_regitered: true
                        }
                        res.status(200).json(response)
                    }
                    else {
                        await new UserModel({
                            mobile: mobile,
                            otp: otp
                        }).save()
                        response = {
                            status_code: 200,
                            message: "Otp sent to " + mobile,
                            is_regitered: false
                        }
                        res.status(200).json(response)
                    }
                });
        }
    },
    LogOut: async (req, res) => {
        if (await UserModel.updateOne(
            {
                _id: req.headers['userid']
            },
            {
                token: ""
            }
        )) {
            res.status(200).json({
                status_code: 200,
                message: "Logout successfull"
            })
        }
        else {
            res.status(200).json({
                status_code: 200,
                message: "Unable to logout"
            })
        }
    },
    VerifyOtp: async (req, res) => {
        const Schema = Joi.object({
            mobile: Joi.number().required(),
            otp: Joi.number().required(),
        })
        var response = {}
        const validatereq = Schema.validate(req.body);
        if (validatereq.error) {
            var response = {
                status_code: 400,
                message: validatereq.error.details[0]['message']
            }
            res.status(400).json(response)
            // console.log(response);
        }
        else {
            // var mobile = await Helper.EnCrypt(req.body.mobile)
            var token = await Helper.GetToken();
            var otp = req.body.otp
            var is_verified = await UserModel.find({ mobile: req.body.mobile, otp: otp })
            if (is_verified.length > 0) {
                await UserModel.updateOne({ mobile: req.body.mobile }, {
                    $set: {
                        otp: "",
                        token: token
                    }
                })
                var user_data = await UserModel.find({ mobile: req.body.mobile }, {})
                response = {
                    status_code: 200,
                    message: "Otp Verified",
                    token: token,
                    user_id: user_data[0]._id
                }
                res.status(200).json(response)
            }
            else {
                response = {
                    status_code: 400,
                    message: "Invalid Otp"
                }
                res.status(400).json(response)
            }
        }
    },
    VerifyForgotPasswordOtp: async (req, res) => {
        const Schema = Joi.object({
            mobile: Joi.number().required(),
            password: Joi.string().regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/).required(),
            otp: Joi.number().required(),
        })
        var response = {}
        const validatereq = Schema.validate(req.body);
        if (validatereq.error) {
            var response = {
                status_code: 400,
                message: validatereq.error.details[0]['message']
            }
            res.status(400).json(response)
        }
        else {
            // var mobile = await Helper.EnCrypt(req.body.mobile)
            var otp = req.body.otp
            var password = req.body.password
            var is_verified = await UserModel.find({ mobile: req.body.mobile, otp: otp })
            if (is_verified.length > 0) {
                var token = await Helper.GetToken();
                await UserModel.updateOne({ mobile: req.body.mobile }, {
                    $set: {
                        otp: "",
                        token: token,
                        password: password,
                    }
                })
                var user_data = await UserModel.find({ mobile: req.body.mobile }, {})
                response = {
                    status_code: 200,
                    message: "Otp Verified Password changed successfully",
                    token: token,
                    user_id: user_data[0]._id
                }
                res.status(200).json(response)
            }
            else {
                response = {
                    status_code: 400,
                    message: "Invalid Otp/Mobile number"
                }
                res.status(400).json(response)
            }
        }
    },
    ResendOtp: async (req, res) => {
        const Schema = Joi.object({
            mobile: Joi.number().required()
        })
        var response = {}
        const validatereq = Schema.validate(req.body);
        if (validatereq.error) {
            var response = {
                status_code: 400,
                message: validatereq.error.details[0]['message']
            }
            console.log(response);
        }
        else {
            const mobile = req.body.mobile;
            // params = {
            //     mobile: mobile,
            //     otp: 1234
            // }
            // var is_sent = await Helper.SMS(params);
            // console.log(is_sent);process.exit();
            await axios.post('https://api.myvfirst.com/psms/servlet/psms.JsonEservice', {
                '@VER': '1.2',
                USER: {
                    '@USERNAME': "embassyxmltrn",
                    '@PASSWORD': 'embsy001',
                    '@UNIXTIMESTAMP': ""
                },
                DLR: { '@URL': "" },
                SMS: [{
                    '@UDH': "0",
                    '@CODING': "1",

                    '@TEXT': "5432 is the One  Time Password(OTP) for your login to Oliveliving. Please do not share with anyone.", "@PROPERTY": "0", "@ID": "1",
                    ADDRESS: [{
                        '@FROM': "EMRESI",
                        '@TO': "916303732109",
                        '@SEQ': "1",
                        '@TAG': ""
                    },
                    ]
                }]
            })
                .then(async (ApiResponse) => {
                    console.log("then")
                    // console.log(is_regitered)
                    response = {
                        status_code: 200,
                        message: "Otp resent to " + mobile,
                        is_regitered: true
                    }
                    res.status(200).json(response)
                    //console.log(response);
                })
                .catch(async (error) => {
                    // console.log(response);
                    // return "response";
                    // console.log(is_regitered)
                    response = {
                        status_code: 200,
                        message: "Otp resent to " + mobile,
                        is_regitered: true
                    }
                    await UserModel.updateOne({ mobile: mobile }, {
                        $set: {
                            otp: 5231
                        }
                    })
                    res.status(200).json(response)
                });
        }
    },
    Forgotpassword: async (req, res) => {
        const schema = Joi.object({
            mobile: Joi.number().required(),
        });
        //.length(10).pattern(/^[0-9]+$/)
        let response = {}
        const validationresult = schema.validate(req.body);
        if (validationresult.error) {
            let message = validationresult.error.details[0]['message'];
            response = {
                'message': message,
                'status_code': 400
            };
            res.status(400).send(response);
        }
        else {
            //Mobile is correct
            cryptedmobile = req.body.mobile// Helper.EnCrypt(req.body.mobile)
            // cryptedmobile1 = Helper.DeCrypt(cryptedmobile)
            // console.log(cryptedmobile1)
            const user = await UserModel.findOne({ mobile: cryptedmobile });
            if (!user) {
                response = {
                    'status_code': 400,
                    'message': 'Mobile number is not valid'
                }
                res.status(400).send(response);
            }
            else {
                await UserModel.updateMany(
                    { mobile: cryptedmobile },
                    {
                        $set: { otp: String(await Helper.GetOTP()) }
                    }
                );
                response = {
                    'status_code': 200,
                    'message': "OTP Send to " + req.body.mobile,
                }
                res.status(200).send(response)
            }
        }
    },
    LoginWithPassword: async (req, res) => {
        const schema = Joi.object({
            mobile: Joi.string().length(10).pattern(/^[0-9]+$/).required(),
            password: Joi.string()
                .required(),
        });
        const validationresult = schema.validate(req.body);
        let response = {};
        if (validationresult.error) {
            let message = validationresult.error.details[0]['message'];
            response = {
                'message': message,
                'status_code': 400
            };
            res.status(400).send(response);
        }
        else {

            cryptedmobile = req.body.mobile;//await Helper.EnCrypt(req.body.mobile)
            const user = await UserModel.find({ mobile: cryptedmobile });
            // console.log(user)
            if (user.length == 0) {
                response = {
                    'status_code': 400,
                    'message': 'Mobile number not registered',
                }
                res.status(400).send(response);
            }
            else {
                const cryptedpassword = req.body.password;//await Helper.EnCrypt(req.body.password);
                // const validPass = await bcrypt.compare(cryptedpassword, user.password);
                if (cryptedpassword != user[0].password) {
                    response = {
                        'status_code': 400,
                        'message': 'Invalid Password',
                    }
                    res.status(400).send(response);
                } else {
                    const Updatetoken = await Helper.GetToken();
                    await UserModel.updateMany(
                        { mobile: cryptedmobile },

                        { $set: { token: Updatetoken } }
                    );
                    response = {
                        'status_code': 200,
                        'message': 'Login Success',
                        'user_id': user._id,
                        'token': Updatetoken
                    }
                    res.status(200).send(response);
                }
            }
            // create and assigned a token
            // const token = jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET);
            // res.header('auth-token', token).send({ authtoken: token });
        }
    },
    Set_User_settings: async (req, res) => {
        //check if the user already exists in database
        //create user
        const schema = Joi.object({
            data: Joi.object().required(),
        });
        const validationresult = schema.validate(req.body);
        let response = {};
        if (validationresult.error) {
            let message = validationresult.error.details[0]['message'];
            response = {
                'message': message,
                'status_code': 400
            }
            res.status(400).json(response)
        } else {
            try {
                const userid = await user_settings.find({ user_id: req.headers['userid'] }, {});
                if (userid.length == 0) {
                    req.body.data.user_id = req.headers['userid']
                    await new user_settings(
                        req.body.data
                    ).save()
                    res.status(200).json({
                        'status_code': 200,
                        'message': 'Success',
                    });
                } else {
                    var where = { _id: req.headers['userid'] }
                    var newvalues = { $set: req.body.data }

                    const updatePost = await user_settings.updateOne(
                        where,
                        newvalues
                    );
                    res.send({
                        'status_code': 200,
                        'message': 'app settings updated'
                    });
                }
            } catch (err) {
                res.status(400).json(err);
            }
        }
    },
    Get_User_Settings: async (req, res) => {
        var userdetails = await user_settings.find({ user_id: req.headers['userid'] }, { __v: 0, _id: 0 })
        // if (userdetails.length == 0) {
        res.status(200).json({
            status_code: 200,
            data: userdetails[0]
            // })
        })
    },
    Logout: async (req, res) => {
        const updatePost = await UserModel.updateOne({ _id: req.headers['userid'] }, {
            $set: {
                token: "",
            }
        })
        res.send({
            'status_code': 200,
            'message': 'User logged out successfully'
        });

    }
}