const router = require('express').Router();
const User = require('../Model/User_master');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const axios = require('axios');
const Helper = require('../Helper/Helper');
// const verify = require('./verifyToken');
const Schedulevisit = require('../Model/Schedulevisit');
const PropertyModel = require('../Model/Properties')
const booking = require('../Model/Booking');
const Joi = require('joi');
const CBAccess_Token = require('../Model/CBAccess_Token')

module.exports = {
    GetHome: async function (req, res) {
        const token = await CBAccess_Token.find({}, {})
        const user_id = req.headers['userid']
        try {
            let ts = Date.now();
            let date_ob = new Date(ts);
            let date = ("0" + date_ob.getDate()).slice(-2);
            let month = ("0" + (date_ob.getMonth() + 1)).slice(-2)
            let year = date_ob.getFullYear();
            // prints date & time in YYYY-MM-DD format
            var current_date = year + "-" + month + "-" + date;
            var booking_details = {
                Current_Bokkings: [],
                Future_Bookings: []
            }
            var userdetails = await User.find({ _id: user_id })
            var visits = await Schedulevisit.find({ user_id: user_id, date: { $gte: current_date } }, {})
            // console.log(visits);
            var booking_reservation = await booking.find({ user_id: user_id })
            var consolidatedvisits = []
            if (visits.length > 0) {
                for (var i = 0; i < visits.length; i++) {
                    var propertydetails1 = await PropertyModel.find({ property_id: visits[i]['property_id'] }, { _id: 0, __v: 0, created_at: 0 })
                    for (var j = 0; j < propertydetails1.length; j++) {
                        consolidatedvisits.push({
                            PropertyID: visits[i]['property_id'],
                            Hotel_Name: propertydetails1[j]['property_name'],
                            Hotel_Location: propertydetails1[j]['lat_long'],
                            Date: visits[i]['date'],
                            time: visits[i]['time'],
                            // visit_id: visits[i]['visit_id']
                        })
                    }
                }
                var reservation_ids = "";
                for (var k = 0; k < booking_reservation.length; k++) {
                    reservation_ids = reservation_ids + "," + booking_reservation[k].reservation_id
                }
                var config = {
                    method: 'get',
                    url: 'https://hotels.cloudbeds.com/api/v1.1/getReservationsWithRateDetails?reservationID=' + reservation_ids.substr(1),
                    headers: {
                        'Authorization': 'Bearer ' +  token[0].access_token,
                        'Cookie': 'acessa_session=3f51e83ea882b6db781cb8e8fbf0f7cf308e5f14; csrf_accessa_cookie=caaae36caae8f107359ad9773765c4d0; identity=satish.y%40theretailinsights.com; remember_code=882e6aa81779194859288aa77f37df93d8fb25aa; HotelLng=en; _ga=GA1.3.2145971734.1609136285'
                    }
                };
                await axios(config)
                    .then(async (response) => {
                        //console.log(JSON.stringify(response.data));
                        for (var k = 0; k < response.data.data.length; k++) {
                            if (response.data.data[k].reservationCheckIn <= current_date && response.data.data[k].reservationCheckOut >= current_date) // Current
                            {
                                var property_details = await booking.aggregate([
                                    {
                                        $match:
                                        {
                                            $and: [
                                                {
                                                    reservation_id: Number(response.data.data[k].reservationID)
                                                }
                                            ]
                                        }
                                    },
                                    {
                                        $lookup: {
                                            from: 'property_masters',
                                            localField: 'property_id',
                                            foreignField: 'property_id',
                                            as: 'property_details'
                                        }
                                    }
                                ]);
                                booking_details.Current_Bokkings.push({
                                    "reservation_id": response.data.data[k].reservationID,
                                    "Property_name": property_details[0].property_details[0].property_name,
                                    "Property_address": property_details[0].property_details[0].address,
                                    "Booked_date": response.data.data[k].dateCreated,
                                    "CheckIn": response.data.data[k].reservationCheckIn,
                                    "CheckOut": response.data.data[k].reservationCheckOut,
                                })
                            } else if (response.data.data[k].reservationCheckIn > current_date)// future
                            {
                                var property_details = await booking.aggregate([
                                    {
                                        $match:
                                        {
                                            $and: [
                                                {
                                                    reservation_id: Number(response.data.data[k].reservationID)
                                                }
                                            ]
                                        }
                                    },
                                    {
                                        $lookup: {
                                            from: 'property_masters',
                                            localField: 'property_id',
                                            foreignField: 'property_id',
                                            as: 'property_details'
                                        }
                                    }
                                ]);
                                booking_details.Future_Bookings.push({
                                    "reservation_id": response.data.data[k].reservationID,
                                    "Property_name": property_details[0].property_details[0].property_name,
                                    "Property_address": property_details[0].property_details[0].address,
                                    "Booked_date": response.data.data[k].dateCreated,
                                    "CheckIn": response.data.data[k].reservationCheckIn,
                                    "CheckOut": response.data.data[k].reservationCheckOut,
                                })
                            }
                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                response1 = {
                    status_code: 200,
                    message: 'Request Successful',
                    Username: userdetails[0]['name'],
                    Schedulevisits: consolidatedvisits,
                    Booking: booking_details
                }
            }
            else {
                response1 = {
                    status_code: 403,
                    Username: userdetails[0]['name'],
                    message: 'No Events'
                }
            }
        } catch (err) {
            response1 = {
                status_code: 403,
                message: err//'Unable to Fetch Events or bookings',
            }
        }
        res.status(response1.status_code).send(response1)
    }

}