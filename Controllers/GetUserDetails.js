const router = require('express').Router();
//const Register = require('../controllers/Registration')
const User = require('../Model/User_master');
const jwt = require('jsonwebtoken');
//const bcrypt = require('bcryptjs');
const axios = require('axios');
const Helper = require('../Helper/Helper');
// const verify = require('./verifyToken');
const Joi = require('joi');
const mongoose= require('mongoose');

module.exports = {
    GetUserDetails: async function (req, res) {
        const schema = Joi.object({
            userid: Joi.string()
                .required(),
        });
        const validationresult = schema.validate(req.body);
        let response = {};
        if (validationresult.error) {
            let message = validationresult.error.details[0]['message'];
            response = {
                'message': message,
                'status_code': 400
            };
            res.status(400).send(response);
        } else {
            const userdetails = await User.find({ _id: req.body.userid })   
            if (!userdetails) {
                response = {
                    'status_code': 400,
                    'message': 'User id is not valid'
                }
                res.status(400).send(response)
            }
            else {
                try{
                res.status(200).send(userdetails);
                }
                catch(err) {
                    res.status(400).send(err);
                }
            }
        }
    }
}