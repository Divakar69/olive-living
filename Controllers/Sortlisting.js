const router = require('express').Router();
const User = require('../Model/User_master');
const Sortlisting = require('../Model/Sortlisting');
const Rooms = require('../Model/Rooms');
const axios = require('axios');
const Joi = require('joi');

module.exports = {
    ShortList: async function (req, res) {

        const token = req.headers['authorization']
        const schema = Joi.object({
            PropertyID: Joi.number().required(),
            room_type_id: Joi.number().required(),
            RoomID: Joi.string().required()
        });
        var response1 = {};
        const validationresult = schema.validate(req.body);
        if (validationresult.error) {
            let message = validationresult.error.details[0]['message'];
            response1 = {
                message: message,
                status_code: 400
            };
            // console.log('Auth If')
            // res.status(400).send(response);
        } else {
            // var shortlist_id = new Date().getTime();
            var user_id = req.headers['userid']
            const sortlisting = new Sortlisting({
                user_id: user_id,
                property_id: req.body.PropertyID,
                room_id: req.body.RoomID,
                room_type_id: req.body.room_type_id,
                shortlist_id: new Date().getTime(),
            });
            var roomdetails = await Rooms.findOne({ room_id: req.body.RoomID }, { _id: 0, __v: 0, created_at: 0 })
            try {
                await sortlisting.save();
                response1 = {
                    status_code: 200,
                    message: "Successfully Shortlisted.Room details: Rent:" + roomdetails.rent + ",Facing:" + roomdetails.facing + ",Floor:" + roomdetails.floor + ",Room Size:" + roomdetails.room_size
                }
            } catch (err) {
                response1 = {
                    status_code: 400,
                    message: err//"Unable to ShortList"
                }
            }
        }
        res.status(response1.status_code).send(response1)
    },
    GetShortLists: async function (req, res) {
        const user_id = req.headers['userid']
        try {
            var List = await Sortlisting.find({
                user_id: user_id
            })
            var consolidatedList = []
            if (List.length > 0) {
                for (var i = 0; i < List.length; i++) {
                    var roomdetails = await Rooms.find({ property_id: List[i]['property_id'], room_id: List[i]['room_id'] })
                    consolidatedList.push({
                        roomimages: roomdetails[0].images,
                        facing: roomdetails[0].facing,
                        floor: roomdetails[0].floor,
                        room_size: roomdetails[0].room_size,
                        rent: roomdetails[0].rent * 30,
                        deposite: (roomdetails[0].rent * 30) * 3,
                        short_list_id : List[i].shortlist_id
                    })
                }
                response1 = {
                    status_code: 200,
                    message: 'Request Successful',
                    Lists: consolidatedList

                }
            }
            else {
                response1 = {
                    status_code: 403,
                    message: 'No ShortLists'
                }
            }
        } catch (err) {
            // res.status(400).send(err); 
            response1 = {
                status_code: 400,
                message: 'Unable To get Short Lists'
            }
        }
        res.status(response1.status_code).send(response1)
    },
    DeleteShortList: async function (req, res) {
        const schema = Joi.object({
            list_id: Joi.number().required()
        });
        var response1 = {};
        const validationresult = schema.validate(req.body);
        if (validationresult.error) {
            let message = validationresult.error.details[0]['message'];
            response1 = {
                message: message,
                status_code: 400
            }
        }
        else {
            const user_id = req.headers['userid']
            try {
                await Sortlisting.deleteOne(
                    { shortlist_id: req.body.list_id, user_id: req.headers['userid'] }
                );
                response1 = {
                    message: "Successfully Removed from List",
                    status_code: 200
                }
                // res.send({ 'message': 'deleted successfuly' });
            } catch (err) {
                // res.status(400).send(err);
                response1 = {
                    message: "Unable to Remove from List",
                    status_code: 400
                }
            }
        }
        res.status(response1.status_code).send(response1)
    }
}
