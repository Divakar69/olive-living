var axios = require('axios');
var qs = require('qs');
const CBAccess_Token = require('../Model/CBAccess_Token')

module.exports = {
    Checkout: async (req, res) => {
        const token = await CBAccess_Token.find({}, {})
        var data = qs.stringify({
            'reservationID': req.body.reservation_id,
            'status': 'checked_out'
        });
        var config = {
            method: 'put',
            url: 'https://hotels.cloudbeds.com/api/v1.1/putReservation',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': `Bearer ${token[0].access_token}`,
                'Cookie': 'acessa_session=588519d7aeee34d0d9c1d5b543fb26959a7a44d4; identity=satish.y%40theretailinsights.com; remember_code=b2be63a890b441c63ed588750f74d2ab89ad701e; user_last_activity=1609134725; AWSALB=clrXJ538gvJpAO/yyC81KjG31c7q4g7IDgP43BDZxfE/AeMUkSKUxTt4rVGvNGG9bC+yG2k7MHHi3aj0TdTvw/dftIQqxIl+MC4E9/kEBo59CuEeZiQefW3KSBcz; AWSALBCORS=clrXJ538gvJpAO/yyC81KjG31c7q4g7IDgP43BDZxfE/AeMUkSKUxTt4rVGvNGG9bC+yG2k7MHHi3aj0TdTvw/dftIQqxIl+MC4E9/kEBo59CuEeZiQefW3KSBcz; HotelDefLng=en; HotelLng=en; _ga=GA1.3.1439040734.1609133287'
            },
            data: data
        };

        await axios(config)
            .then(function (response) {
                // console.log(JSON.stringify(response.data));
                if (response.data.success) {
                    res.status(200).json({
                        status_code: 200,
                        data: response.data.data
                    })
                }else
                {
                    res.status(400).json({
                        status_code: 400,
                        data: response.data.message
                    })
                }
            })
            .catch(function (error) {
                // console.log(error);
                res.status(200).json({
                    status_code: 200,
                    error: error
                })
            });

    }
}