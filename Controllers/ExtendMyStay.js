const router = require('express').Router();
const Joi = require('joi');
var axios = require('axios');
var qs = require('qs');
var FormData = require('form-data');
const Booking = require('../Model/Booking');
var data = new FormData();
const CBAccess_Token = require('../Model/CBAccess_Token')

module.exports = {
    ExtendStay: async (req, res) => {
        const schema = Joi.object({
            // HotelName: Joi.string().required(),
            // HotelAddress: Joi.string().required(),
            // HotelCity: Joi.string().required(),
            // HotelState: Joi.string().required(),
            // HotelPhone: Joi.string().required(),
            // HotelEmail: Joi.string().required(),
            reservation_id: Joi.number().required(),
            rooms: Joi.array().required(),
            estimatedArrivalTime: Joi.string().empty("")
        })
        const validationresult = schema.validate(req.body);
        if (validationresult.error) {
            let message = validationresult.error.details[0]['message'];
            response1 = {
                'message': message,
                'status_code': 400
            }
        } else {
            var property_details = await Booking.aggregate([
                {
                    $match:
                    {
                        $and: [
                            {
                                reservation_id: Number(req.body.reservation_id)
                            }
                        ]
                    }
                },
                {
                    $lookup: {
                        from: 'property_masters',
                        localField: 'property_id',
                        foreignField: 'property_id',
                        as: 'property_details'
                    }
                }
            ])
            // console.log(JSON.stringify(property_details));process.exit()
            const req_rooms = req.body.rooms;
            var rooms = [];
            for (var i = 0; i < req_rooms.length; i++) {
                rooms.push({
                    roomTypeID: req_rooms[i]['roomTypeID'],
                    adults: req_rooms[i]['adults'],
                    children: req_rooms[i]['children'],
                    rateID: req_rooms[i]['rateID'],
                    checkinDate: req_rooms[i]['checkinDate'],
                    checkoutDate: req_rooms[i]['checkoutDate'],
                    subReservationID: req_rooms[i]['subReservationID']
                });
            }
            var data = qs.stringify({
                reservationID: req.body.reservation_id,
                estimatedArrivalTime: req.body.estimatedArrivalTime,
                rooms
            });
            const token = await CBAccess_Token.find({}, {})
            var config = {
                method: 'put',
                url: 'https://hotels.cloudbeds.com/api/v1.1/putReservation',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': 'Bearer ' + token[0].access_token,
                    'Cookie': 'HotelLng=en; _ga=GA1.3.1439040734.1609133287; acessa_session=588519d7aeee34d0d9c1d5b543fb26959a7a44d4; identity=satish.y%40theretailinsights.com; user_last_activity=1609134725; remember_code=b2be63a890b441c63ed588750f74d2ab89ad701e; _gid=GA1.3.1509355398.1612175816; csrf_accessa_cookie=d79ded33e0573194f5d322a790914b39; HotelDefLng=en; AWSALB=aD7Flw2eKKhdoIKwUM2i0Txg486ac5+AVD+aLmh+/33yUEn1TDE0S01NbSvMwq1o8fnNQmG+MMsBSVw0kvUNn9WGoQJY1964sDxqT9w0tkW+eJFIyNp2BQTSPeAKe923LENg8kuZ5SjYFaOxnBlpf4O1t275oV3GurMxclX8zUnGCswOEbxKymkUj8L8sw==; AWSALBCORS=aD7Flw2eKKhdoIKwUM2i0Txg486ac5+AVD+aLmh+/33yUEn1TDE0S01NbSvMwq1o8fnNQmG+MMsBSVw0kvUNn9WGoQJY1964sDxqT9w0tkW+eJFIyNp2BQTSPeAKe923LENg8kuZ5SjYFaOxnBlpf4O1t275oV3GurMxclX8zUnGCswOEbxKymkUj8L8sw=='
                },
                data: data
            };

            await axios(config)
                .then(async (response) => {
                    // let transport = nodemailer.createTransport({
                    //     host: 'smtp.office365.com',
                    //     port: 587,
                    //     auth: {
                    //         user: 'olivehelpdesk@oliveliving.com',
                    //         pass: 'Ol!ve@2021$#'
                    //     }
                    // });
                    // const message = {
                    //     from: 'olivehelpdesk@oliveliving.com', // Sender address
                    //     to: req.body.user_email,         // List of recipients
                    //     subject: 'Your visit to  ' + property_details[0].property_details[0].property_name + '   is scheduled -Conformation #' + req.body.reservation_id, // Subject line
                    //     html: '<p>' + property_details[0].property_details[0].property_name + ' </p><p>Thank you for choosing &nbsp;' + property_details[0].property_details[0].property_name + ',located at &nbsp;' + property_details[0].property_details[0].address.addr1 + '&nbsp;in' + property_details[0].property_details[0].address.addr2 + ',&nbsp;' + property_details[0].property_details[0].address.addr3 + '&nbsp;for your stay.We have successfully confirmed your reservation extension and  have issued you the following conformation number:&nbsp;' + req.body.reservation_id + ' </p><br><p>If you need to make changes or require assistance please call &nbsp;' + 6303732109 + '&nbsp;or email us at &nbsp;' + "olive@mosham.com" + '.We look forward to your stay.</p><br><p>Once again,If you need to make changes or require assistance please call &nbsp;' + 6303732109 + '&nbsp; or email us at &nbsp;' + "olive@mosham.com" + '&nbsp;.We look forward to your stay.</p><br><br><p>Thank you for your reservation!</p><p>' + property_details[0].property_details[0].property_name + '</p>'
                    // };

                    // transport.sendMail(message, function (err, info) {
                    //     if (err) {
                    //         console.log(err)
                    //     } else {
                    //         console.log(info);
                    //     }
                    // });
                    response1 = {
                        status_code: 200,
                        data: response.data
                    }
                })
                .catch(async (error) => {
                    response1 = {
                        status_code: 400,
                        data: error
                    }
                });
        }
        res.status(response1.status_code).send(response1)
    }
}