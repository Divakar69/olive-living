const router = require('express').Router();
const Register = require('../controllers/Registration')
const User = require('../model/User');
const Loginotp = require('../model/Loginotp');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const axios = require('axios');
const Helper = require('../Helper/Helper');
// const verify = require('./verifyToken');
const Joi = require('joi');

module.exports = {
    LoginWithotp: async function (req, res) {
        const schema = Joi.object({

            mobile: Joi.string().length(10).pattern(/^[0-9]+$/).required(),

        });
        const validationresult = schema.validate(req.body);
        let response = {}
        if (validationresult.error) {
            let message = validationresult.error.details[0]['message'];

            let response = {

                'message': message,
                'status_code': 400
            };
            // res.status(400).send(response);
        }
        else {
            // res.status(200).send("Shiva");

            cryptedmobile = await Helper.EnCrypt(req.body.mobile)

            const user = await User.findOne({ mobile: cryptedmobile });
            if (!user) {
                response = {
                    'status_code': 400,
                    'message': 'Mobile number is not valid'
                }
                // res.status(400).send(response);
            } else {
                var otp = await Helper.GetOTP();
                // console.log(otp);
                var response1= {};
                var config = {
                    method: 'get',
                    url: 'http://olive.theretailinsights.co/api/v1/vendor/56',
                    headers: { }
                  };
                   await axios(config)
                  .then(function (result) {
                      response1 = {
                          data : result.data
                      }
                    //console.log(JSON.stringify(response.data));
                  })
                  .catch(function (error) {
                    console.log('else')
                    console.log(error);
                  });
                await axios.post(response1.data.data.vendor.vendor_url, {
                    '@VER': '1.2',
                    USER: {
                        '@USERNAME': response1.data.data.vendor.username,
                        '@PASSWORD': response1.data.data.vendor.password,


                        '@UNIXTIMESTAMP': ""
                    },
                    DLR: { '@URL': "" },
                    SMS: [{
                        '@UDH': "0",
                        '@CODING': "1",

                        '@TEXT': "5432 is the One  Time Password(OTP) for your login to Oliveliving. Please do not share with anyone.", "@PROPERTY": "0", "@ID": "1",
                        ADDRESS: [{
                            '@FROM': response1.data.data.vendor.Address,
                            '@TO': req.body.mobile,

                            '@SEQ': "1",
                            '@TAG': ""
                        },
                        ]
                    }]
                })

                    .then(async function (ApiResponse) {


                        // console.log("mobile",req.body.mobile);
                        // console.log(Math.floor(Math.random() * (9999 - 1000)) + 1000);
                        // process.exit
                        const isupdated = await User.updateMany(
                            { mobile: cryptedmobile },
                            { $set: { otp: otp } }
                        );
                        response = {
                            'status_code': 200,
                            'message': "OTP send to " + req.body.mobile
                        }
                        // res.status(200).send(response);
                    })
                    .catch(async (error) => {
                        // console.log("error", error);
                        response = {
                            'status_code': 304,
                            'message': "Unable to send OTP"
                        }
                        // res.status(304).send(response);
                    });
            }

        }
        const status_code = response.status_code;
        // console.log(response.status_code);
        // res.status(response.status_code).send(response)
        res.send(response);
    }
}