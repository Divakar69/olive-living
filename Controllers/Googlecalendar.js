const router = require('express').Router();
const fs = require('fs');
const readline = require('readline');
const {google} = require('googleapis');
const OAuth2 =google.auth.OAuth2;
const credentials = require('../cerdentials.json')


// const verify = require('./verifyToken');

const Joi = require('joi');



module.exports = {
  
    Getcalendar: async function (req, res) {
      const schema = Joi.object({
        Date: Joi.string(),
      });
      const scopes = ['https://www.googleapis.com/auth/calendar https://www.googleapis.com/auth/calendar.events']

      const client = new google.auth.GoogleAuth({
        credentials,
        scopes,
      })
      
     // client.subject = 'google-calendar@olive-living.iam.gserviceaccount.com'

//Create a new instance of oAuth and set our Client ID & Client Secret.
      //  const oauth2Client = new OAuth2(
      //   '1029991630995-h9n64g84j636vvho661m4noqmrt68qcf.apps.googleusercontent.com',
      //   'wfEDYDy8FJTdi9wMu4_joe8E'
      // );
      // oauth2Client.setCredentials({
      //   refresh_token: '1//04Bbjo4asJU_1CgYIARAAGAQSNwF-L9Irq49lVYG3kAZXEYzB5mcJ0XBsXr4sghx24J-3ESdvfD7Z7jtZyCKY9ABG4AMEBhSpYoM'
      // })
        
      const calendar = google.calendar({ version: 'v3', auth: client })

// Create a new event start date instance for temp uses in our calendar.
const eventStartTime =  new Date(req.body.Date)
console.log(eventStartTime);


// Create a new event end date instance for temp uses in our calendar.
const eventEndTime =new Date(req.body.Date)
eventEndTime.setMinutes(eventEndTime.getMinutes() + 60)
console.log(eventEndTime);

// Create a dummy event for temp uses in our calendar
const event = {
  summary: `Meeting with David`,
  location: `3595 California St, San Francisco, CA 94118`,
  description: `Meet with David to talk about the new client project and how to integrate the calendar for booking.`,
  colorId: 1,
  start: {
    dateTime: eventStartTime,
    timeZone: 'Asia/Kolkata',
  },
  end: {
    dateTime: eventEndTime,
    timeZone: 'Asia/Kolkata',
  },
  attendees: [
    {'email': 'nupur@theretailinsights.com'},
  ],
  reminders: {
    'useDefault': false,
    'overrides': [
      {'method': 'popup', 'minutes': 10},
    ],
  },
}

calendar.events.insert({
  sendNotifications: true,
  calendarId: 'primary',
  resource: event,
}, function(err, event) {
  if (err) {
    console.log('There was an error contacting the Calendar service: ' + err);
    return;
  }
  console.log('Event created.');
});

// Check if we a busy and have an event on our calendar for the same time.
// calendar.freebusy.query(
//   {
//     resource: {
//       timeMin: eventStartTime,
//       timeMax: eventEndTime,
//       timeZone: 'Asia/Kolkata',
//       items: [{ id: 'primary' }],
//     },
//   },
//   (err, res) => {
//     // Check for errors in our query and log them if they exist.
//     if (err) return console.error('Free Busy Query Error: ', err)

//     // Create an array of all events on our calendar during that time.
//     const eventArr = res.data.calendars.primary.busy

//     // Check if event array is empty which means we are not busy
//     if (eventArr.length === 0)
//       // If we are not busy create a new calendar event.
//       return calendar.events.insert(
//         { calendarId: 'primary', resource: event },
//         err => {
//           // Check for errors and log them if they exist.
//           if (err) return console.error('Error Creating Calender Event:', err)
//           // Else log that the event was created.
//           return console.log('Calendar event successfully created.')
//         }
//       )

//     // If event array is not empty log that we are busy.
//     return console.log(`Sorry I'm busy...`)
//   }
// )
}
}
