const fs = require('fs');
const path = require('path');
const assert = require('assert');
const { PDFDocument } = require('pdf-lib');

module.exports = {
    AddImage: async (params) => {
        const pdfDoc = await PDFDocument.load(fs.readFileSync(params.pdf));//pdf path
        try {
            console.log(params.imagetype.toLowerCase())
            var img = ""
            if (params.imagetype.toLowerCase() == 'image/jpg' || params.imagetype.toLowerCase() == 'image/jpeg') {
                img = await pdfDoc.embedJpg(fs.readFileSync(params.signaturepath));//image path
            } else if (params.imagetype.toLowerCase() == 'image/png') {
                img = await pdfDoc.embedPng(fs.readFileSync(params.signaturepath));//image path
            }
            // for (let i = 0; i < pdfDoc.getPageCount(); i++) {
            let imagePage = '';
            // imagePage = pdfDoc.getPage(i);
            imagePage = pdfDoc.getPage(pdfDoc.getPageCount() - 1);
            // console.log(i + 1)
            // console.log(imagePage.getWidth())
            let xx = imagePage.getWidth()
            // console.log(imagePage.getHeight())
            let yy = imagePage.getHeight()
            imagePage.drawImage(img, {
                x: xx - 550,
                y: yy - 150,
                width: 100,
                height: 40
            });
            // }
            const pdfBytes = await pdfDoc.save();
            const newFilePath = `${path.basename(params.reservationid, '.pdf')}-result.pdf`;
            // console.log(newFilePath)
            fs.writeFileSync(params.signedrentalpath + newFilePath, pdfBytes);
            fs.unlinkSync(params.signaturepath)
            return {
                success: true,
                path: params.signedrentalpath + newFilePath
            }
        }
        catch (err) {
            return {
                success: false,
            }
        }
    }
}