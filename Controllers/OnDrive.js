const oneDriveAPI = require('onedrive-api');
var OneDriveAuth = require('onedrive-auth');
var fs = require("fs");
var csv = require("csv-parser");
var OneDriveAuth = require('onedrive-auth');

// create OneDrive client
// function testonedrive() {
//     var onedrive = new OneDriveAuth({
//         clientId: 'a19c0d8a-efea-46ee-80ef-b3dc05e911b4',//'YOUR-CLIENT-ID',
//         scopes: 'onedrive.readonly wl.signin',//'onedrive.readonly wl.signin',
//         redirectUri: 'https://login.microsoftonline.com/common/oauth2/nativeclient'//'YOUR-CALLBACK-URI',
//     });

//     onedrive.auth().then(token => {
//         // call OneDrive API endpoints with given token
//         console.log(token)
//     }).catch(err => {
//         console.log(err)
//         // create auth button
//     });
// }
// testonedrive()

module.exports = {
    // OndriveGetAccesstoken: async (req, res) => {
    //     var onedrive = new OneDriveAuth({
    //         clientId: 'a19c0d8a-efea-46ee-80ef-b3dc05e911b4',
    //         // clientSecret : "571a5d83-a323-4748-97c3-2f8fa832a582",
    //         scopes: 'onedrive.readonly offline_access',
    //         redirectUri: 'http://localhost:9000/api/OnDriveCallBack'//'https://login.microsoftonline.com/common/oauth2/nativeclient',
    //     });
    //     // check for active token
    //     onedrive.auth().then(token => {
    //         console.log("success");
    //         // call OneDrive API endpoints with given token
    //     }).catch(err => {
    //         console.log("fail");
    //         // create auth button
    //     });
    // },
    // GetAccesstoken: async (req, res) => {
    //     const config = {
    //         client: {
    //             id: 'a19c0d8a-efea-46ee-80ef-b3dc05e911b4',
    //             secret: '571a5d83-a323-4748-97c3-2f8fa832a582'
    //         },
    //         auth: {
    //             tokenHost: 'https://api.oauth.com'
    //         }
    //     };
    //     const client = new AuthorizationCode(config);

    //     // console.log(client)
    // },
    // OnDriveCallBack: async (req, res) => {
    //     console.log("Im Here")
    // },
    RoomsFileDownload: async (req, res) => {
        const fileStream = oneDriveAPI.items.download({
            accessToken: "Bearer " + process.env.OndriveToken,
            itemId: '2385EED47681663A!137'
        });
        const file = fs.createWriteStream('rooms.xlsx')
        fileStream.pipe(file);
        res.status(200).json({
            status_code: 200,
            message: "File Downloaded"
        })
    },
    PrpertiesFileDownload: async (req, res) => {
        const fileStream = oneDriveAPI.items.download({
            accessToken: "Bearer " + process.env.OndriveToken,
            itemId: '2385EED47681663A!136'
        });
        const file = fs.createWriteStream('properties.xlsx')
        fileStream.pipe(file);
        res.status(200).json({
            status_code: 200,
            message: "File Downloaded"
        })
    }
}