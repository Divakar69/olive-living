const Booking = require('../Model/Booking')
var axios = require('axios');
var nodemailer = require('nodemailer');
const Joi = require('joi');
var FormData = require('form-data');
var axios = require('axios');
var qs = require('qs');
var data = new FormData();
const CBAccess_Token = require('../Model/CBAccess_Token')

module.exports = {
    Booking: async (req, res) => {
        const schema = Joi.object({
            orderid: Joi.string().required(),
            first_name: Joi.string().required(),
            last_name: Joi.string().required(),
            gender: Joi.string().required(),
            startDate: Joi.date().required(),
            endDate: Joi.date().required(),
            guestCountry: Joi.string().required(),
            property_id: Joi.string().required(),
            guestZip: Joi.string().length(6).pattern(/^[0-9]+$/).required(),
            guestEmail: Joi.string().email({ tlds: { allow: false } }).required(),
            guestPhone: Joi.string().length(10).pattern(/^[0-9]+$/).required(),
            rooms: Joi.array().required(),
            adults: Joi.array().required(),
            children: Joi.array().required(),
            paymentMethod: Joi.string().required()
        });
        const validationresult = schema.validate(req.body);
        if (validationresult.error) {
            let message = validationresult.error.details[0]['message'];
            response1 = {
                'message': message,
                'status_code': 400
            }
        } else {
            var response1 = {};
            const first_name = req.body.first_name
            const last_name = req.body.last_name
            const gender = req.body.gender
            const startDate = req.body.startDate
            const endDate = req.body.endDate
            const guestCountry = req.body.guestCountry
            const guestZip = req.body.guestZip
            const guestEmail = req.body.guestEmail
            const guestPhone = req.body.guestPhone
            const rooms_count = req.body.rooms
            const adults_count = req.body.adults
            const children_count = req.body.children
            const paymentMethod = req.body.paymentMethod
            const property_id = req.body.property_id
            const user_id = req.headers['userid']
            // console.log(adults_count[0]['roomTypeID']);
            // process.exit()
            const token = await CBAccess_Token.find({}, {})
            // console.log(token[0].access_token)
            data.append('startDate', startDate);
            data.append('endDate', endDate);
            data.append('guestFirstName', first_name);
            data.append('guestLastName', last_name);
            data.append('guestGender', gender);
            data.append('guestCountry', guestCountry);
            data.append('guestZip', guestZip);
            data.append('guestEmail', guestEmail);
            data.append('guestPhone', guestPhone);
            for (var i = 0; i < rooms_count.length; i++) {
                data.append('rooms[' + i + '][roomTypeID]', rooms_count[i]['roomTypeID']);
                data.append('rooms[' + i + '][quantity]', rooms_count[i]['quantity']);
                data.append('rooms[' + i + '][roomRateID]', rooms_count[i]['roomRateID']);
                data.append('rooms[' + i + '][roomID]', rooms_count[i]['roomID']);
            }
            // console.log(adults_count.length);
            for (var i = 0; i < adults_count.length; i++) {
                data.append('adults[' + i + '][roomID]', adults_count[i]['roomID']);
                data.append('adults[' + i + '][roomTypeID]', adults_count[i]['roomTypeID']);
                data.append('adults[' + i + '][quantity]', adults_count[i]['quantity']);
            }
            for (var i = 0; i < children_count.length; i++) {
                data.append('children[' + i + '][roomTypeID]', children_count[i]['roomTypeID']);
                data.append('children[' + i + '][roomID]', children_count[i]['roomID']);
                data.append('children[' + i + '][quantity]', children_count[i]['quantity']);
            }
            data.append('paymentMethod', paymentMethod);
            var config = {
                method: 'post',
                url: 'https://hotels.cloudbeds.com/api/v1.1/postReservation',
                headers: {
                    'Authorization': 'Bearer ' + token[0].access_token,
                    'Cookie': 'HotelLng=en; _ga=GA1.3.1439040734.1609133287; _gid=GA1.3.320964563.1609133287; acessa_session=588519d7aeee34d0d9c1d5b543fb26959a7a44d4; identity=satish.y%40theretailinsights.com; remember_code=642f8e622bac86afa862132b7e84a9663844c5be; user_last_activity=1609134725; AWSALB=TL2AG5ivVXGt5G/YErvejbko1zXLB79qvLJdKOknJNh2QRcSbmzgUoPUULg+HcffKTUuxnkmr8V+TQ+IWFPcomziE8EpBv+8ltveYOQOeWosFakQuBYKlntmjAQKqRu3pqWj1XxFWgN0SBxE2SrzfaNL062/Do9Dg+/at2xnU+cUMOC0/1zBSgnPkRLa1A==; AWSALBCORS=TL2AG5ivVXGt5G/YErvejbko1zXLB79qvLJdKOknJNh2QRcSbmzgUoPUULg+HcffKTUuxnkmr8V+TQ+IWFPcomziE8EpBv+8ltveYOQOeWosFakQuBYKlntmjAQKqRu3pqWj1XxFWgN0SBxE2SrzfaNL062/Do9Dg+/at2xnU+cUMOC0/1zBSgnPkRLa1A==; HotelDefLng=en',
                    ...data.getHeaders()
                },
                data: data
            };
            await axios(config)
                .then(async (response) => {
                    var guests = 0;
                    // console.log(response);process.exit()
                    if (response.data.success) {
                        // var reservationdata = {};
                        // for (var i = 0; i < response.data.unassigned.length; i++) {
                        //     response.data.unassigned[i]['roomrateID'] = rooms_count[i]['roomRateID']
                        // }
                        // const reserve = new Reservations({
                        //     user_id: user_id,
                        //     reservation_id: response.data.reservationID,
                        //     resevation_data: JSON.stringify(response.data),
                        //     guest_id: response.data.guestID,
                        //     start_date: response.data.startDate,
                        //     end_date: response.data.endDate,
                        //     no_guests: guests,
                        //     grandtotal: response.data.grandTotal,
                        //     status: response.data.status,
                        // });
                        await Booking.updateOne({
                            user_id: user_id,
                            payorderid: req.body.orderid
                        },
                            {
                                $set: {
                                    property_id: property_id,
                                    reservation_id: response.data.reservationID,
                                    cbbookingstatus: "success"
                                }
                            }
                        )
                        // try {
                        //     // const is_reserved = reserve.save()
                        //     var propertydetails = await Booking.find({ property_id: property_id }, {})
                        //     let transport = nodemailer.createTransport({
                        //         host: 'smtp.office365.com',
                        //         port: 587,
                        //         auth: {
                        //             user: 'olivehelpdesk@oliveliving.com',
                        //             pass: 'Ol!ve@2021$#'
                        //         }
                        //     });
                        //     const message = {
                        //         from: 'olivehelpdesk@oliveliving.com', // Sender address
                        //         to: req.body.guestEmail,         // List of recipients
                        //         subject: 'Your visit to  ' + propertydetails[0].property_name + '   is scheduled -Conformation #' + reserve.reservation_id, // Subject line
                        //         html: '<p>' + propertydetails[0].property_name + ' </p><p>Thank you for choosing &nbsp;' + propertydetails[0].property_name + ',located at &nbsp;' + propertydetails[0].address[0].addr3 + '&nbsp;in' + propertydetails[0].address[0].addr2 + ',&nbsp;' + propertydetails[0].address[0].addr1 + '&nbsp;for your stay.We have successfully confirmed your reservation and have issued the following conformation number:&nbsp;' + reserve.reservation_id + ' </p><br><p>If you need to make changes or require assitance please call &nbsp;' + propertydetails[0].property_phone + '&nbsp;or email us at &nbsp;' + propertydetails[0].property_email + '.We look forward to your stay.</p><br><p>Once again,If you need to make changes or require assistance please call &nbsp;' + propertydetails[0].property_phone + '&nbsp; or email us at &nbsp;' + propertydetails[0].property_email + '&nbsp;.We look forward to your stay.</p><br><br><p>Thank you for your reservation!</p><p>' + propertydetails[0].property_name + '</p>'
                        //     };
                        //     transport.sendMail(message, function (err, info) {
                        //         console.log("email")
                        //         if (err) {
                        //             console.log(err)
                        //         } else {
                        //             console.log(info);
                        //         }
                        //     });
                        //     response1 = {
                        //         status_code: 200,
                        //         data: response.data
                        //     }
                        // } catch (err) {
                        //     console.log('Email catch')
                        //     // response1 = {
                        //     //     status_code: 400,
                        //     //     data: err
                        //     // }
                        // }
                        response1 = {
                            status_code: 200,
                            data: response.data
                        }
                    }
                    else {
                        console.log('Success catch')
                        response1 = {
                            status_code: 400,
                            data: response.data
                        }
                    }
                })
                .catch((error) => {
                    console.log('Booking catch')
                    response1 = {
                        status_code: 400,
                        data: error
                    }
                });
        }
        res.status(response1.status_code).send(response1)
    },
    CancelBooking: async (req, res) => {
        var reservation_id = req.body.reservation_id
        var data = qs.stringify({
            'reservationID': reservation_id,
            //'rooms[0][roomTypeID]': '299801',
            //'rooms[0][adults]': '1',
            //'rooms[0][children]': '0',
            //'rooms[0][rateID]': '733257',
            //'rooms[0][checkinDate]': '2021-04-01',
            //'rooms[0][checkoutDate]': '2021-07-01',
            //'estimatedArrivalTime': '08:00',
            //'rooms[0][subReservationID]': '172301438658',
            'status': 'canceled'
        });
        const token = await CBAccess_Token.find({}, {})
        var config = {
            method: 'put',
            url: 'https://hotels.cloudbeds.com/api/v1.1/putReservation',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Bearer ' + token[0].access_token,
                'Cookie': 'acessa_session=588519d7aeee34d0d9c1d5b543fb26959a7a44d4; identity=satish.y%40theretailinsights.com; remember_code=b2be63a890b441c63ed588750f74d2ab89ad701e; user_last_activity=1609134725; AWSALB=iLdIttXnJmfR3rgQd9c1Hx+LjT5h3ksQVirCMnMqO2M1dacAlK1Gu+YhQ3MHHl8OHyF71QljPpllsgp8P3OKuGrJKPrPlBm/wzukiNOMOTmE6zUjaOF9X1f8Ppg7; AWSALBCORS=iLdIttXnJmfR3rgQd9c1Hx+LjT5h3ksQVirCMnMqO2M1dacAlK1Gu+YhQ3MHHl8OHyF71QljPpllsgp8P3OKuGrJKPrPlBm/wzukiNOMOTmE6zUjaOF9X1f8Ppg7; HotelLng=en; _ga=GA1.3.1439040734.1609133287'
            },
            data: data
        };

        axios(config)
            .then(async (response) => {
                // console.log(JSON.stringify(response.data));
                let data = response.data.data
                res.status(200).json(data)
            })
            .catch(function (error) {
                // console.log(error);
                res.status(400).json(error)
            });

    },
    Pay: async (req, res) => {
        var orderid = req.body.orderid
        data.append('reservationID', req.body.reservationID)
        data.append('type', req.body.type)
        data.append('amount', req.body.amount)
        data.append('description', req.body.description)
        // data.append('reservationID', params.reservationID);
        // data.append('type', params.type);
        // data.append('amount', params.amount);
        // data.append('description', params.description);
        const token = await CBAccess_Token.find({}, {})
        var config = {
            method: 'post',
            url: 'https://hotels.cloudbeds.com/api/v1.1/postPayment',
            headers: {
                'Authorization': 'Bearer ' + token[0].access_token,
                'Cookie': 'acessa_session=588519d7aeee34d0d9c1d5b543fb26959a7a44d4; csrf_accessa_cookie=d90a0626c77492a9f6ccf7173b0cc50a; identity=satish.y%40theretailinsights.com; remember_code=b2be63a890b441c63ed588750f74d2ab89ad701e; user_last_activity=1609134725; HotelDefLng=en; HotelLng=en; _ga=GA1.3.1439040734.1609133287',
                ...data.getHeaders()
            },
            data: data
        };
        await axios(config)
            .then(async (response) => {
                // console.log(JSON.stringify(response.data));
                await Booking.updateOne({
                    user_id: req.headers['userid'],
                    payorderid: orderid,
                    reservation_id: req.body.reservationID
                },
                    {
                        $set: {
                            cbtranstatus: "success",
                            cbtranid: response.data.transactionID
                        }
                    }
                )
                res.status(200).json({
                    status_code: 200,
                    transactionID: response.data.transactionID,
                    orderid: orderid
                })
            })
            .catch(function (error) {
                console.log(error);
            });
    },
    GetInvoice: async (req, res) => {
        const schema = Joi.object({
            reservationID: Joi.number().required()
        });
        const validationresult = schema.validate(req.body);
        if (validationresult.error) {
            let message = validationresult.error.details[0]['message'];
            res.status(400).json({
                'message': message,
                'status_code': 400
            })
        } else {
            const token = await CBAccess_Token.find({}, {})
            var config = {
                method: 'get',
                url: 'https://hotels.cloudbeds.com/api/v1.1/getReservationInvoiceInformation?reservationID=' + req.body.reservationID,
                headers: {
                    'Authorization': 'Bearer ' + token[0].access_token,
                    'Cookie': 'acessa_session=588519d7aeee34d0d9c1d5b543fb26959a7a44d4; csrf_accessa_cookie=1dfea74824d94191ef01291d28b2858b; identity=satish.y%40theretailinsights.com; remember_code=b2be63a890b441c63ed588750f74d2ab89ad701e; user_last_activity=1609134725; AWSALB=t5CRFY0BXDmf3IVqE1WO2kaj/zjJVD8cvzDPnjV0guNrDZqP2ISm+qiCXhS5l/jDeEhYMX5Np/jDDql9hgC1wmResYw+Sc/178C09fO3XK+zChGVVnjsAuHc8gCz; AWSALBCORS=t5CRFY0BXDmf3IVqE1WO2kaj/zjJVD8cvzDPnjV0guNrDZqP2ISm+qiCXhS5l/jDeEhYMX5Np/jDDql9hgC1wmResYw+Sc/178C09fO3XK+zChGVVnjsAuHc8gCz; HotelDefLng=en; HotelLng=en; _ga=GA1.3.1439040734.1609133287'
                }
            };
            await axios(config)
                .then(function (response) {
                    console.log(JSON.stringify(response.data));
                    res.status(200).json(
                        {
                            status_code: 200,
                            data: response.data.data
                        })
                })
                .catch(function (error) {
                    console.log(error);
                    res.status(400).json({
                        status_code: 400,
                        message: error
                    })
                });
        }
    }
}