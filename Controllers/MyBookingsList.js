const express = require('express');
const Joi = require('joi');
const axios = require('axios');
const Booking = require('../Model/Booking');
const Properties = require('../Model/Properties');
const Rooms = require('../Model/Rooms');
const CBAccess_Token = require('../Model/CBAccess_Token')

module.exports = {
    PostBookins: async (req, res) => {
        await new Booking({
            user_id: "123456788",
            reservation_id: "919702548320"
        }).save();
    },
    GetReservations: async (req, res) => {
        const token = await CBAccess_Token.find({}, {})
        var reservations = await Booking.find({
            user_id: req.headers['userid']
        }, {})
        // console.log(reservation_ids); process.exit();
        var reservation_ids = ""
        if (reservations.length > 0) {
            for (var i = 0; i < reservations.length; i++) {
                reservation_ids = reservation_ids + "," + reservations[i].reservation_id
            }
            //console.log(reservation_ids.substr(1));process.exit()
        }
        else {
            res.status(400).json({
                status_code: 400,
                message: "No Reservations found yet"
            })
        }
        var config = {
            method: 'get',
            url: "https://hotels.cloudbeds.com/api/v1.1/getReservationsWithRateDetails?reservationID=" + reservation_ids.substr(1),
            headers: {
                'Authorization': 'Bearer ' +  token[0].access_token,
                'Cookie': 'acessa_session=588519d7aeee34d0d9c1d5b543fb26959a7a44d4; csrf_accessa_cookie=e8a204cd19efcdfe60e6b50695555f15; identity=satish.y%40theretailinsights.com; remember_code=b2be63a890b441c63ed588750f74d2ab89ad701e; user_last_activity=1609134725; HotelDefLng=en; HotelLng=en; _ga=GA1.3.1439040734.1609133287'
            }
        };

        axios(config)
            .then(async (response) => {
                // console.log(JSON.stringify(response.data));
                var data = response.data;
                // var property_details = await Properties.find({}, { _id: 0, __v: 0 })
                // console.log(property_details);
                var guests = 0
                for (var i = 0; i < data.data.length; i++) {
                    var property_details = await Booking.aggregate([
                        {
                            $match:
                            {
                                $and: [
                                    {
                                        reservation_id: Number(data.data[i].reservationID)
                                    }
                                ]
                            }
                        },
                        {
                            $lookup: {
                                from: 'property_masters',
                                localField: 'property_id',
                                foreignField: 'property_id',
                                as: 'property_details'
                            }
                        }
                    ])
                    for (var j = 0; j < data.data[i].rooms.length; j++) {
                        var room_details = await Rooms.find({ room_type_id: data.data[i].rooms[j].roomTypeID, room_id: data.data[i].rooms[j].roomID })

                        data.data[i].rooms[j].is_private = room_details[0].is_private
                        guests = guests + Number(data.data[i].rooms[j].adults) + Number(data.data[i].rooms[j].children)
                    }
                    data.data[i].roomscount = data.data[i].rooms.length
                    data.data[i].no_guests = guests
                    data.data[i].property_name = property_details[0].property_details[0].property_name
                    // console.log(JSON.stringify(data.data))
                   data.data[i].rentpermonth = 30000//(room_details[0].rent) * 30
                    data.data[i].deposite = 600000//((room_details[0].rent) * 30) * 3
                    // console.log(JSON.stringify(data.data))
                }
                res.status(200).json({
                    status_code: 200,
                    data: data.data
                })
            })
            .catch(function (error) {
                // console.log(error);
                res.status(500).json({
                    status_code: 500,
                    data: error
                })
            });
    }
}