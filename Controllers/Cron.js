var csv = require("csv-parser");
var fs = require("fs");
var Helper = require("../Helper/Helper");
var axios = require('axios');
var Properties = require('../Model/Properties');
var Rooms = require('../Model/Rooms');
var OnDrive = require('./OnDrive');
const CBAccess_Token = require('../Model/CBAccess_Token')

async function Getproperties() {
    const token = await CBAccess_Token.find({}, {})
    var result = {}
    var config = {
        method: 'get',
        url: 'https://hotels.cloudbeds.com/api/v1.1/getHotelDetails',
        headers: {
            'Authorization': 'Bearer ' + token[0].access_token,
            'Cookie': 'acessa_session=588519d7aeee34d0d9c1d5b543fb26959a7a44d4; csrf_accessa_cookie=cccb0993ceef4b623322852671302821; identity=satish.y%40theretailinsights.com; remember_code=b2be63a890b441c63ed588750f74d2ab89ad701e; user_last_activity=1609134725; AWSALB=LGm5rKKViphabml6MCSPJXWkMM0brauAT/S/AsFUcCgKKUOD/BpSWOl9Wb/aIihhRF0NDuwiODXO/2TgIMQrNvBBsHWQxF/MbyBH60HThqSNijRD7K0ZHbfxGlUC; AWSALBCORS=LGm5rKKViphabml6MCSPJXWkMM0brauAT/S/AsFUcCgKKUOD/BpSWOl9Wb/aIihhRF0NDuwiODXO/2TgIMQrNvBBsHWQxF/MbyBH60HThqSNijRD7K0ZHbfxGlUC; HotelDefLng=en; HotelLng=en; _ga=GA1.3.1439040734.1609133287'
        }
    };

    await axios(config)
        .then(function (response) {
            // console.log(JSON.stringify(response.data));
            result = {
                data: response.data.data
            }
        })
    return result
}

async function ValidateMandateFields(mandatefields, csvmandatefields) {
    let errormsg = ""
    for (let i = 0; i < mandatefields.length; i++) {
        if (!csvmandatefields.includes(mandatefields[i])) {
            errormsg = errormsg + "," + mandatefields[i]
        }
    }
    if (errormsg) {
        errormsg = errormsg + " are missing."
    }
    return errormsg
}
async function PropertyMaterUpdate() {
    var CBproperties = await Getproperties()
    var a = 0;
    var errormsg = "";
    let mandatefileds = [
        "property_name",
        "community_manager_mobile",
        "community_manager_email",
        "nearby_locations",
        "deposite",
        "minmum_stay"
    ]
    var insertarray = []
    var DBProperties = await Properties.find({}, {})
    var updatefilter = []
    var updatequery = []
    var params = {
        OneDriveAccess_token: process.env.OndriveToken,
        itemID: '2385EED47681663A!136',
        filename: 'properties.xlsx'
    }
    var id_downloaded = await Helper.OndriveFileDownload(params)
    var xlsxarray = await Helper.XLSXtoObjectOfArrays({
        xlspath: "./properties.xlsx"
    })
    // res.status(200).json(xlsxarray)
    // process.exit()
    // fs.createReadStream("./properties.xlsx").pipe(csv()).on('data', async (row) => {
    var keys = Object.keys(xlsxarray[0].rowvalues)
    // let csvfields = Object.keys(row)
    errormsg = await ValidateMandateFields(mandatefileds, keys)
    if (errormsg) {
        let params = {
            email: "divakar@theretailinsights.com",
            // csvname: "Property",
            subject: "jhiushiuahdiuahiuhde",
            html: '<p>Hi &nbsp;' + "Divakar" + ',</p><p>Below is the error in &nbsp;' + params.csvname + "CSV" + '</p><br><p>' + params.errormsg + '</p></p><br><p>Please update the csv:&nbsp;</p><br><br><p>Thank you!</p><p>Developer</p>'
        }
        await Helper.Email(params)
        // res.status(400).json({
        //     status_code: 400,
        //     message: "Some error occurred"
        // })
        console.log("Some error occurred")
    }
    else {
        // xlsxarray.forEach(async row => {
        var CBpropertiesarr = []
        CBpropertiesarr.push(
            CBproperties.data
        )
        for (var i = 0; i < xlsxarray.length; i++) {
            var row = xlsxarray[i].rowvalues
            // console.log(row)
            // console.log(CBproperties)
            // for (var i = 0; i < CBproperties.data.length; i++) {
            if (!row.property_name && !row.community_manager_mobile && !row.community_manager_email && !row.nearby_locations && !row.deposite && !row.minmum_stay) // Enters if all 
            { }
            else {
                let MatchedProperty = CBpropertiesarr.find((Name) => Name.propertyName === row.property_name) //returns matched object
                if (MatchedProperty) {
                    let DBMatchedProperty = DBProperties.find((DB_prop_name) => DB_prop_name.property_name === MatchedProperty.propertyName) //DB properties matched
                    if (!DBMatchedProperty) {
                        if (MatchedProperty) {
                            insertarray.push({
                                property_id: MatchedProperty.propertyID,
                                property_name: MatchedProperty.propertyName,
                                property_email: row.community_manager_email,
                                property_phone: row.community_manager_mobile,
                                about: MatchedProperty.propertyDescription,
                                lat_long: MatchedProperty.propertyAddress.propertyLatitude + "," + MatchedProperty.propertyAddress.propertyLongitude,
                                property_images: MatchedProperty.propertyImage,
                                nearby: row.nearby_locations.slice(","),
                                amenities: MatchedProperty.propertyAmenities,
                                address: MatchedProperty.propertyAddress,
                            })
                        }
                        else {
                            errormsg = + "," + row.property_name + " is not matched with cloudbeds data"
                        }
                    }
                    else {
                        if (MatchedProperty) {
                            // await Properties.updateMany
                            updatefilter.push({ property_id: DBMatchedProperty.property_id })
                            updatequery.push({
                                property_name: MatchedProperty.propertyName,
                                property_email: row.community_manager_email,
                                property_phone: row.community_manager_mobile,
                                about: MatchedProperty.propertyDescription,
                                lat_long: MatchedProperty.propertyAddress.propertyLatitude + "," + MatchedProperty.propertyAddress.propertyLongitude,
                                property_images: MatchedProperty.propertyImage,
                                nearby: row.nearby_locations.slice(","),
                                amenities: MatchedProperty.propertyAmenities,
                                address: MatchedProperty.propertyAddress
                            })
                        }
                        else {
                            errormsg = + "," + row.property_name + " is not matched with cloudbeds data"
                        }
                    }
                }
                else {
                    errormsg = + "," + row.property_name + " is not matched with cloudbeds data"
                }
            }
            // }
        }
        if (errormsg) {
            let params = {
                email: "divakar@theretailinsights.com",
                csvname: "Property",
                errormsg: errormsg
            }
            await Helper.Email(params)
            console.log("Error occurred Mail sent to divakar@theretailinsights.com")
        } else {
            if (insertarray.length > 0) {
                await Properties.insertMany(insertarray)
            }
            if (updatefilter.length > 0) {
                var i = 0
                for (filter of updatefilter) {
                    await Properties.updateOne(filter, updatequery[i])
                    i++
                }
            }
            // res.status(200).json({
            //     status_code: 200,
            //     message: "properties updated"
            // })
            console.log("properties updated")
        }
    }
    return true
}
async function RoomsMasterUpdate() {
    const token = await CBAccess_Token.find({}, {})
    var params = {
        method: 'get',
        url: 'https://hotels.cloudbeds.com/api/v1.1/getRooms',
        access_token: token[0].access_token
    }
    var CBrooms = await Helper.CallAPI_Axios(params)
    var params = {
        method: 'get',
        url: 'https://hotels.cloudbeds.com/api/v1.1/getRoomTypes?detailedRates=True',
        access_token: token[0].access_token
    }
    var CBroomTypes = await Helper.CallAPI_Axios(params)
    // console.log(CBroomTypes,CBrooms);process.exit()
    // res.status(200).json({
    //     rooms: CBrooms,
    //     roomtypes: CBroomTypes
    // })
    // process.exit()
    var params = {
        method: 'get',
        url: 'https://hotels.cloudbeds.com/api/v1.1/getHotelDetails',
        access_token: token[0].access_token
    }
    var errormsg = "";
    var CBProperties = await Helper.CallAPI_Axios(params)

    var DBRooms = await Rooms.find({}, {})
    var DBProperties = await Properties.find({}, {})

    if ((CBrooms.success != undefined && CBrooms.success) && (CBroomTypes.success != undefined && CBroomTypes.success)) {// if got rooms
        // var id_downloaded = await OnDrive.RoomsFileDownload() // Download Rooms File from Onedrive
        // process.exit()
        // if (id_downloaded) {
        var XLSXRooms = await Helper.XLSXtoObjectOfArrays({ xlspath: "./rooms.xlsx" })
        let mandatefileds = [
            "property_name",
            "room_type_name",
            "room_name",
            "facing",
            "price",
            "size"
        ]
        var keys = Object.keys(XLSXRooms[0].rowvalues)
        errormsg = await ValidateMandateFields(mandatefileds, keys)
        if (errormsg) {
            // let params = {
            //     email: "divakar@theretailinsights.com",
            //     csvname: "Rooms",
            //     errormsg: errormsg
            // }
            // await Helper.Email(params)
            // res.status(400).json({
            //     status_code: 400,
            //     message: "Some error occurred"
            // })
            console.log("Some error occurred")
        }
        else {
            var insertarr = []
            var updatequery = []
            var updatefilter = []
            if (XLSXRooms.length > 0) {
                for (var i = 0; i < XLSXRooms.length; i++) {
                    var row = XLSXRooms[i].rowvalues
                    let MatchedProperty = DBProperties.find((Name) => Name.property_name === row.property_name) //Check XLSX property name in DB
                    if (MatchedProperty) { //Should Match
                        let Is_Room_Exists = DBRooms.find((RoomTypeName) => (RoomTypeName.room_type_name === row.room_type_name) && (RoomTypeName.property_id === MatchedProperty.property_id) && (RoomTypeName.room_name === row.room_name)) //Check XLSX property name and Room Type Name in DB
                        if (Is_Room_Exists) // if Found Then Update
                        {
                            var CBroomsarr = []
                            CBroomsarr.push(
                                CBrooms.data
                            )
                            let PropertyMatchedInCBRooms = CBroomsarr.find((Property) => (Property[0].propertyID == MatchedProperty.property_id)) //Check XLSX property name in CB for propertyID
                            if (PropertyMatchedInCBRooms) {
                                let Rooms = PropertyMatchedInCBRooms[0].rooms
                                let MatchedRooms = Rooms.find((Room) => (Room.roomTypeName == row.room_type_name) && (Room.roomName == row.room_name))// Check room in CB with combination of property id and room type name and room name
                                if (MatchedRooms) {
                                    var CBRoomTypesData = CBroomTypes.data
                                    var RoomTypes = CBRoomTypesData.find((RoomTypes) => (RoomTypes.roomTypeID == MatchedRooms.roomTypeID) && (RoomTypes.propertyID == MatchedProperty.property_id))
                                    updatefilter.push({
                                        property_id: MatchedProperty.property_id,
                                        room_type_id: RoomTypes.roomTypeID,
                                        room_id: MatchedRooms.roomID,
                                    })
                                    updatequery.push({
                                        room_type_name: row.room_type_name,
                                        rent: row.price,
                                        images: RoomTypes.roomTypePhotos,
                                        is_private: MatchedRooms.isPrivate,
                                        facing: row.facing,
                                        floor: row.floor,
                                        room_size: row.size,
                                        amenities: RoomTypes.roomTypeFeatures,
                                        max_guest: RoomTypes.maxGuests,//
                                        adultsIncluded: RoomTypes.adultsIncluded,
                                        childrenIncluded: RoomTypes.childrenIncluded,
                                        rommTypeDescription: RoomTypes.roomTypeDescription
                                    })
                                }
                                else {
                                    errormsg = errormsg + "," + row.room_name + " is not matching with CB rooms types list"
                                }
                            }
                            else {
                                errormsg = errormsg + "," + row.property_name + " is not matching with CB rooms list"
                            }
                        }
                        else // Else insert
                        {
                            var CBroomsarr = []
                            CBroomsarr.push(
                                CBrooms.data
                            )
                            if (CBroomsarr.length > 0) {
                                let PropertyMatchedInCBRooms = CBroomsarr.find((Property) => (Property[0].propertyID == MatchedProperty.property_id)) //Check XLSX property name in CB for propertyID
                                if (PropertyMatchedInCBRooms) {
                                    let Rooms = PropertyMatchedInCBRooms[0].rooms
                                    let MatchedRooms = Rooms.find((Room) => (Room.roomTypeName === row.room_type_name) && (Room.roomName === row.room_name))// Check room in CB with combination of property id and room type name and room name
                                    if (MatchedRooms) {
                                        var CBRoomTypesData = CBroomTypes.data
                                        var RoomTypes = CBRoomTypesData.find((RoomTypes) => (RoomTypes.roomTypeID == MatchedRooms.roomTypeID) && (RoomTypes.propertyID == MatchedProperty.property_id))
                                        insertarr.push(
                                            {
                                                property_id: MatchedProperty.property_id,
                                                room_type_id: MatchedRooms.roomTypeID,
                                                room_type_name: MatchedRooms.roomTypeName,
                                                room_id: MatchedRooms.roomID,
                                                rent: row.price,
                                                images: RoomTypes.roomTypePhotos,
                                                is_private: MatchedRooms.isPrivate,
                                                facing: row.facing,
                                                floor: row.floor,
                                                room_size: row.size,
                                                amenities: RoomTypes.roomTypeFeatures,
                                                max_guest: RoomTypes.maxGuests,
                                                adultsIncluded: RoomTypes.adultsIncluded,
                                                childrenIncluded: RoomTypes.childrenIncluded,
                                                rommTypeDescription: RoomTypes.roomTypeDescription
                                            }
                                        )
                                    }
                                    else {
                                        errormsg = + ", " + `${errormsg} ,${row.room_name} is not matching with CB room types`
                                    }
                                }
                                else {
                                    errormsg = + ", " + `${errormsg} ,${row.property_name} is not matching with CB rooms`
                                }
                            }
                            else {
                                errormsg = errormsg + ",No rooms found in CB"
                            }
                        }
                    }
                    else {
                        errormsg = errormsg + ",This property is not available in Database"
                    }
                }
            }
            else {
                errormsg = errormsg + ",File is empty"
            }
            if (errormsg) {
                let params = {
                    email: "divakar@theretailinsights.com",
                    csvname: "Rooms",
                    errormsg: errormsg.substr(1)
                }
                await Helper.Email(params)
                console.log("Error occurred Mail sent to divakar@theretailinsights.com")
            }
            else {
                if (insertarr.length > 0) {
                    await Rooms.insertMany(insertarr)
                }
                if (updatefilter.length > 0) {
                    var i = 0
                    for (filter of updatefilter) {
                        await Properties.updateOne(filter, updatequery[i])
                        i++
                    }
                }
                // res.status(200).json({
                //     status_code: 200,
                //     message: "Rooms updated"
                // })
                console.log("Rooms updated")
                // return true
            }
        }
    }
    else {
        // res.status(200).json({
        //     status_code: 400,
        //     message: "Something went wrong"
        // })
        console.log("Something went wrong")
    }
    return true
}

async function MainFun() {
}

module.exports = {
    UpdateMasters : async (req, res) => {
        await PropertyMaterUpdate()
        await RoomsMasterUpdate()
        res.status(200).json({
            status_code: 200,
            message: "Masters updated"
        })
    }
}
// MainFun()
