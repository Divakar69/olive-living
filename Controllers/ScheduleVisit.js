const router = require('express').Router();
const User = require('../Model/User_master');
const PropertyModel = require('../Model/Properties')
const Schedulevisit = require('../Model/Schedulevisit');
const axios = require('axios');
const Joi = require('joi');
var nodemailer = require('nodemailer');
const {google} = require('googleapis');
const OAuth2 =google.auth.OAuth2;

module.exports = {
    Postvisit: async (req, res) => {
        new Schedulevisit({
            user_id: "123456788",
            property_name: "Oliva Stars",
            visit_id: "1a3wedw243e",
            property_id: "181961",
            room_id: "123456-0",
            room_type_id: "123456",
            date: "2021-02-26",
            time: "9:00 PM"
        }).save()
    },
    SetVisit: async function (req, res) {
        // const token = req.headers['authorization']
        const schema = Joi.object({
            // user_id: Joi.string().required(),
            PropertyID: Joi.number().required(),
            Room_type_id: Joi.number().required(),
            room_id: Joi.string().required(),
            Date: Joi.string().required(),
            Time: Joi.string().required()
        });
        // let utc=Date.parse(req.body.Date)/1000;
        // console.log(utc);
        // process.exit();
        var response1 = {};
        const validationresult = schema.validate(req.body);
        if (validationresult.error) {
            let message = validationresult.error.details[0]['message'];
            response1 = {
                message: message,
                status_code: 400
            };
            // console.log('Auth If')
            // res.status(400).send(response);
        } else {
            const user_id = req.headers['userid']
            var userdata = await User.find({ _id: user_id }, {})
            const schedulevisit = new Schedulevisit({
                user_id: user_id,
                date: req.body.Date,
                time: req.body.Time,
                property_id: req.body.PropertyID,
                room_id: req.body.PropertyID,
                room_type_id: req.body.PropertyID,
                visit_id: new Date().getTime()
            });
            var propertydetails = await PropertyModel.findOne({ property_id: req.body.PropertyID }, { _id: 0, __v: 0, created_at: 0 })
            var  axios = require('axios');
            var response2= {};      
                var config = {
                method: 'get',
                url: 'http://olive.theretailinsights.co/api/v1/vendor/57',
                headers: { }
                };

               await  axios(config)
                .then(function (result) {
                    response2 = {
                        data : result.data
                    }
                //console.log(JSON.stringify(response2.data));
                })
                .catch(function (error) {
                console.log(error);
                });
            //console.log(JSON.stringify(response2.data));
            const oauth2Client = new OAuth2(
                response2.data.data.vendor.client_id,
                response2.data.data.vendor.client_secert
              );
              oauth2Client.setCredentials({
                refresh_token: response2.data.data.vendor.refersh_token
              })
                
              const calendar = google.calendar({ version: 'v3', auth: oauth2Client })
        
        // Create a new event start date instance for temp uses in our calendar.
        const eventStartTime =  new Date(req.body.Date)
        
        // Create a new event end date instance for temp uses in our calendar.
        const eventEndTime =new Date(req.body.Date)
        eventEndTime.setMinutes(eventEndTime.getMinutes() + 60)
        // Create a dummy event for temp uses in our calendar
        const event = {
          summary: `Scheduled a visit`,
          location: propertydetails.property_name ,
          description: `For a visiting the property`,
          colorId: 1,
          start: {
            dateTime: eventStartTime,
            timeZone: 'Asia/Kolkata',
          },
          end: {
            dateTime: eventEndTime,
            timeZone: 'Asia/Kolkata',
          },
          attendees: [
            {'email': userdata[0].email},
          ],
          reminders: {
            'useDefault': false,
            'overrides': [
              {'method': 'popup', 'minutes': 10},
            ],
          },
        }
        
        calendar.events.insert({
          sendNotifications: true,
          calendarId: 'primary',
          resource: event,
        }, function(err, event) {
          if (err) {
            console.log('There was an error contacting the Calendar service: ' + err);
            return;
          }
          console.log('Event created.');
        });
            let transport = nodemailer.createTransport({
                host: 'smtp.office365.com',
                port: 587,
                auth: {
                    user: 'olivehelpdesk@oliveliving.com',
                    pass: 'Ol!ve@2021$#'
                }
            });
            const message = {
                from: 'olivehelpdesk@oliveliving.com', // Sender address
                to: userdata[0].email,         // List of recipients
                subject: 'Your visit to  ' + propertydetails.property_name + '   is scheduled -Conformation #' + schedulevisit.visit_id, // Subject line
                html: '<p>Hi &nbsp;' + userdata[0].name + ',</p><p>Thank you for choosing &nbsp;' + propertydetails.property_name + '.Your Property visit is  scheduled successfully.</p><br><p>Here are the  visiting schedule details:</p><p>Visit Date:&nbsp;' + schedulevisit.date + '</p><p>Visit Date:&nbsp;' + schedulevisit.time + '</p><br><p>Address:&nbsp;' + req.body.HotelAddress + '</p><p>If you need to make  changes or require assistance   please call &nbsp;' + req.body.HotelPhone + '&nbsp;or email us at &nbsp;' + req.body.HotelEmail + '.</p><br><p>We look forward to meet you soon.</p><p>' + propertydetails.property_name + '</p><br><br><p>Thank you!</p><p>Community Manager</p>'
            };

            transport.sendMail(message, function (err, info) {
                if (err) {
                    console.log(err)
                } else {
                    console.log(info);
                }
            });
            try {
                await schedulevisit.save();
                response1 = {
                    status_code: 200,
                    message: "Your property visit is successfully scheduled."
                }
            } catch (err) {
                response1 = {
                    status_code: 200,
                    message: err//"Unable to set Scheduled Visit for " + req.body.PropertyID
                }
            }
        }
        res.status(response1.status_code).send(response1)
    },
    GetVisits: async function (req, res) {
        const user_id = req.headers['userid']
        try {
            var visits = await Schedulevisit.find({ user_id: user_id })
            var consolidatedvisits = []
            if (visits.length > 0) {
                for (var i = 0; i < visits.length; i++) {
                    var propertydetails = await PropertyModel.find({ property_id: visits[i]['property_id'] }, {})
                    consolidatedvisits.push({
                        Property_name: propertydetails[0].property_name,
                        latlong: propertydetails[0].lat_long,
                        property_email: propertydetails[0].property_email,
                        property_phone: propertydetails[0].property_phone,
                        Date: visits[i]['date'],
                        time: visits[i]['time'],
                        visit_id: visits[i]['visit_id']
                    })
                }
                response1 = {
                    status_code: 200,
                    message: 'Request Successful',
                    visits: consolidatedvisits
                }
            }
            else {
                response1 = {
                    status_code: 403,
                    message: 'No Scheduled Visits Found Yet'
                }
            }
        } catch (err) {
            response1 = {
                status_code: 403,
                message: 'Unable to Fetch Scheduled visits',
            }
        }
        res.status(response1.status_code).send(response1)
    },
    DeleteVisit: async function (req, res) {
        const schema = Joi.object({
            visit_id: Joi.string()
                .required()
        });
        const validationresult = schema.validate(req.body);
        if (validationresult.error) {
            let message = validationresult.error.details[0]['message'];
            response1 = {
                message: message,
                status_code: 400
            };
            // console.log('Auth If')
            // res.status(400).send(response);
        }
        else {
            const user_id = req.headers['userid']
            try {
                const updatePost = await Schedulevisit.deleteOne(
                    { visit_id: req.body.visit_id }
                );
                response1 = {
                    status_code: 200,
                    message: 'Deleted successfully',
                }
            } catch (err) {
                response1 = {
                    status_code: 400,
                    message: 'Unable to delete visit',
                }
            }
        }
        res.status(response1.status_code).send(response1)
    },
    GetAvailableVisitSlots: async (req, res) => {
        const schema = Joi.object({
            selected_date: Joi.string().empty(""),
            property_id: Joi.string().empty(""),
        });
        const validationresult = schema.validate(req.body);
        let response = {};
        if (validationresult.error) {
            let message = validationresult.error.details[0]['message'];
            response = {
                'message': message,
                'status_code': 400
            }
            res.status(400).json(response)
        } else {
            var selected_date = req.body.selected_date
            var property_id = req.body.property_id
            var TimeSlots = [
                "08:00 AM",
                "09:00 AM",
                "10:00 AM",
                "11:00 AM",
                "12:00 AM",
                "01:00 PM",
                "02:00 PM",
                "03:00 PM",
                "04:00 PM",
                "05:00 PM"
            ]
            var TimeSlots_with_limit = {
                "08:00 AM": 4,
                "09:00 AM": 4,
                "10:00 AM": 4,
                "11:00 AM": 4,
                "12:00 AM": 4,
                "01:00 PM": 4,
                "02:00 PM": 4,
                "03:00 PM": 4,
                "04:00 PM": 4,
                "05:00 PM": 4
            }
            for (var i = 0; i < TimeSlots.length; i++) {
                var SlotsBooked = await Schedulevisit.aggregate([
                    {
                        $match: {
                            $and: [
                                {
                                    date: selected_date,
                                    property_id: property_id,
                                    time: TimeSlots[i]
                                }
                            ]
                        }
                    },
                    {
                        $group: {
                            _id: "$time",
                            count: { $sum: 1 }
                        }
                    }
                ])
                // console.log(SlotsBooked)
                if (SlotsBooked.length > 0) {
                    TimeSlots_with_limit[TimeSlots[i]] = TimeSlots_with_limit[TimeSlots[i]] - SlotsBooked[0].count
                }
            }
            // console.log(TimeSlots_with_limit)
            response = {
                status_code: 200,
                data: TimeSlots_with_limit
            }
            res.status(200).json(response)
        }
    }
}