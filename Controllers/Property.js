const express = require('express');
const PropertyModel = require('../Model/Properties')
const RoomsModel = require('../Model/Rooms')
const Joi = require('joi');
const Rooms = require('../Model/Rooms');
const axios = require('axios');
const { componentsToColor } = require('pdf-lib');
const CBAccess_Token = require('../Model/CBAccess_Token')
const Helper = require('../Helper/Helper')
const date = require('date-and-time');
const now = new Date();
const users = require('../Model/User_master')

module.exports = {
    GetProperties: async (req, res) => {
        const schema = Joi.object({
            occupency: Joi.string().empty(""),
            startdate: Joi.string().empty(""),
            enddate: Joi.string().empty("")
        });
        let response = {}
        var rooms = {};
        var onwords = {};
        var data = [];
        const validationresult = schema.validate(req.body);
        if (validationresult.error) {
            let message = validationresult.error.details[0]['message'];
            response = {
                'message': message,
                'status_code': 400
            };
        }
        else {
            start_date = req.body.startdate//"2021-02-20"
            end_date = req.body.enddate//"2021-02-22"
            occupency = req.body.occupency
            var properties = await PropertyModel.find({}, { _id: 0, __v: 0 });
            properties.forEach(async property => {
                var rooms = await RoomsModel.aggregate([
                    {
                        $match: {
                            $or: [
                                {
                                    is_booked: false,
                                    occupency: occupency,
                                    property_id: property.property_id
                                },
                                {
                                    is_booked: true,
                                    checkin_date: { $not: { $gte: start_date } },
                                    checkout_date: { $not: { $gte: end_date } },
                                    occupency: occupency,
                                    property_id: property.property_id
                                }
                            ]
                        }
                    },
                    {
                        $group: {
                            _id: "$occupency",
                            count: { $sum: 1 }
                        }
                    }
                ])
                if (rooms.length > 0) {
                    var onwords = await RoomsModel.aggregate(
                        [
                            {
                                $match: {
                                    $or: [
                                        {
                                            is_booked: false,
                                            occupency: occupency,
                                            property_id: property.property_id
                                        },
                                        {
                                            is_booked: true,
                                            checkin_date: { $not: { $gte: start_date } },
                                            checkout_date: { $not: { $gte: end_date } },
                                            occupency: occupency,
                                            property_id: property.property_id
                                        }
                                    ]
                                }
                            },
                            {
                                $group:
                                {
                                    _id: "$property_id",
                                    onwords: { $min: "$rent" }
                                }
                            }
                        ]);
                    data.push({
                        property_id: property.property_id,
                        property_name: property.property_name,
                        property_images: property.property_images,
                        address: property.address,
                        lat_long: property.lat_long,
                        amenities: property.amenities.length,
                        total_units: rooms.length,
                        occupency: {
                            type: rooms[0]._id,
                            units: rooms[0].count
                        },
                        price_starts_from: onwords[0].onwords
                    })
                }
            })
            console.log(data); process.exit();
        }
        res.status(200).json({ data: data });
        // var properties = await PropertyModel.find({}, { _id: 0, __v: 0 });
        // res.status(200).send(properties);
    },
    PostProperty: async (req, res) => {
        var address = [];
        var obj = {
            'addr1': "City",
            'addr2': "Mdl",
            'addr3': "Vlg"
        }
        address.push(obj);

        amenities = [
            "Swimming Pool",
            "Gym",
            "Saloon",
            "Bakery",
            "Beauty Parlour"
        ]
        var nearby = [
            {
                place: "Airport",
                distance: 2
            },
            {
                place: "Hospital",
                distance: 1
            },
            {
                place: "TechParks",
                distance: 5
            }
        ]
        new PropertyModel({
            property_id: 123456,
            property_name: "Heven",
            property_images: "path1,path2",
            lat_long: "123456,123456",
            address: address,
            amenities: amenities,
            nearby: nearby,
            about: "Description"
        }).save();

        // process.exit()
    },
    PropertyDetails: async (req, res) => {
        const schema = Joi.object({
            property_id: Joi.number().required()
        });
        let response = {}
        const validationresult = schema.validate(req.body);
        if (validationresult.error) {
            let message = validationresult.error.details[0]['message'];
            response = {
                'message': message,
                'status_code': 400
            };
        }
        else {
            var property_id = req.body.property_id
            var propertydetails = await PropertyModel.findOne({ property_id: property_id }, { _id: 0, __v: 0, created_at: 0 })
            var roomtypes = await RoomsModel.aggregate([
                [
                    {
                        $match: {
                            property_id: property_id
                        }
                    },
                    {
                        $group: {
                            _id: "$occupency",
                            count: { $sum: 1 }
                        }
                    }
                ]
            ])
            var data = {
                propertydetails,
                "roomtypes": roomtypes
            }
            // propertydetails.roomtypes = roomtypes;
            response = {
                status_code: 200,
                data: data
            }
        }
        res.status(response.status_code).json(response);
    },
    GetPropertyList: async (req, res) => {
        const schema = Joi.object({
            startdate: Joi.string().required(),
            enddate: Joi.string().required(),
            is_private: Joi.boolean().required(),

            adults: Joi.number().integer().min(1).required(),
            children: Joi.number().integer().min(0).required(),

            minrate: Joi.number().integer().min(0).required(),
            maxrate: Joi.number().integer().min(0).required(),

            pageNumber: Joi.number().required(),
            pageSize: Joi.number().required(),
        });
        var result = {}
        const validationresult = schema.validate(req.body);
        if (validationresult.error) {
            res.status(400).json({
                'message': validationresult.error.details
            })
        }
        else {
            var checkwithcurrentdate = await Helper.ComapreDates(date.format(now, 'DD/MM/YYYY'), new Date(req.body.startdate))
            if (checkwithcurrentdate) {
                var is_datesvalid = await Helper.ComapreDates(date.format(new Date(req.body.startdate), 'DD/MM/YYYY'), date.format(new Date(req.body.enddate), 'DD/MM/YYYY'))
                if (checkwithcurrentdate) {
                    var startdate = req.body.startdate
                    var enddate = req.body.enddate
                    var is_private = req.body.is_private
                    var pageNumber = req.body.pageNumber
                    var pageSize = req.body.pageSize
                    var adults = req.body.adults
                    var children = req.body.children
                    var minrate = req.body.minrate
                    var maxrate = req.body.maxrate

                    var property_id = await PropertyModel.find({}, { _id: 0, __v: 0 })
                    var property_id_str = "";
                    property_id.forEach(id => {
                        property_id_str = property_id_str + "," + id.property_id
                    });
                    // console.log("https://hotels.cloudbeds.com/api/v1.1/getAvailableRoomTypes?propertyIDs=" + property_id_str.substr(1) + "&startDate=" + startdate + "&endDate=" + enddate + "&pageNumber=" + pageNumber + "&pageSize=" + pageSize); process.exit();
                    if (minrate > 0 && maxrate > minrate && adults > 0 && children >= 0) {
                        url = "https://hotels.cloudbeds.com/api/v1.1/getAvailableRoomTypes?propertyIDs=" + property_id_str + "&startDate=" + startdate + "&endDate=" + enddate + "&adults=" + adults + "&children=" + children + "&minRate=" + minrate + "&maxRate=" + maxrate + "&pageNumber=" + pageNumber + "&pageSize=" + pageSize
                    } else if (minrate == 0 && maxrate == 0 && adults > 0 && children >= 0) {
                        url = "https://hotels.cloudbeds.com/api/v1.1/getAvailableRoomTypes?propertyIDs=" + property_id_str + "&startDate=" + startdate + "&endDate=" + enddate + "&adults=" + adults + "&children=" + children + "&pageNumber=" + pageNumber + "&pageSize=" + pageSize
                    }
                    else if (minrate > 0 && maxrate > minrate && adults == 1 && children == 0) {
                        url = "https://hotels.cloudbeds.com/api/v1.1/getAvailableRoomTypes?propertyIDs=" + property_id_str + "&startDate=" + startdate + "&endDate=" + enddate + "&pageNumber=" + pageNumber + "&pageSize=" + pageSize
                    }
                    const token = await CBAccess_Token.find({}, {})
                    // console.log(token)
                    var config = {
                        method: 'get',
                        url: url,
                        headers: {
                            'Authorization': 'Bearer ' + token[0].access_token,
                            'Cookie': 'acessa_session=588519d7aeee34d0d9c1d5b543fb26959a7a44d4; csrf_accessa_cookie=72ead5e547d8554b08b98446f8adf658; identity=satish.y%40theretailinsights.com; remember_code=b2be63a890b441c63ed588750f74d2ab89ad701e; user_last_activity=1609134725; HotelDefLng=en; HotelLng=en; _ga=GA1.3.1439040734.1609133287'
                        }
                    };
                   await axios(config)
                        .then(async (response) => {
                            var property_ids = response.data.data
                            // console.log(JSON.stringify(response.data.success)); process.exit();
                            if (response.data.success) {
                                var propertyids_list = [];
                                property_ids.forEach(property => {
                                    propertyids_list.push(
                                        property.propertyID
                                    )
                                });
                                // console.log(propertyids_list);
                                var resultobj = {
                                    available: {},
                                    notavailable: {}
                                }
                                var properties_list = await PropertyModel.find({ property_id: { $in: propertyids_list } })
                                // var properties_list = await PropertyModel.find({}, {})
                                var data = [];
                                var minrent_list = [];
                                for (var i = 0; i < properties_list.length; i++) {
                                    for (var j = 0; j < property_ids.length; j++) {
                                        if (properties_list[i].property_id == property_ids[j].propertyID) {
                                            var totunits = 0
                                            var occupencycount = 0;
                                            for (var k = 0; k < property_ids[j].propertyRooms.length; k++) {
                                                totunits = totunits + property_ids[j].propertyRooms[k].roomsAvailable
                                                var ocupencydata = await RoomsModel.find({
                                                    property_id: properties_list[i].property_id,
                                                    room_type_id: property_ids[j].propertyRooms[k].roomTypeID,
                                                    is_private: is_private
                                                })
                                                // occupencycount = occupencycount + ocupencydata.length
                                                var onwords = await RoomsModel.aggregate([
                                                    {
                                                        $match: {
                                                            $and: [
                                                                {
                                                                    property_id: properties_list[i].property_id,
                                                                    room_type_id: property_ids[j].propertyRooms[k].roomTypeID,
                                                                    is_private: is_private
                                                                }
                                                            ]
                                                        }
                                                    },
                                                    {
                                                        $group:
                                                        {
                                                            _id: "$room_type_id",
                                                            onwords: { $min: "$rent" }
                                                        }
                                                    }
                                                ]);
                                                if (onwords.length > 0) {
                                                    minrent_list.push(onwords[0].onwords);
                                                }
                                            }
                                            data.push({
                                                property_id: property_ids[j].propertyID,
                                                property_name: properties_list[i].property_name,
                                                property_address: properties_list[i].address,
                                                property_lat_long: properties_list[i].lat_long,
                                                property_images: properties_list[i].property_images,
                                                property_amenities: properties_list[i].amenities.length,
                                                total_units: totunits,
                                                is_private: is_private,
                                                // occupency: occupencycount,
                                                price_starts_from: Math.min(...minrent_list)
                                            })
                                        }
                                    }
                                }
                                resultobj.available = data
                                var data = [];
                                var minrent_list = [];
                                var properties_list = await PropertyModel.find({ property_id: { $nin: propertyids_list } })
                                var data = [];
                                var minrent_list = [];
                                for (var i = 0; i < properties_list.length; i++) {
                                    // for (var j = 0; j < property_ids.length; j++) {
                                    // if (properties_list[i].property_id == property_ids[j].propertyID) {
                                    var totunits = 0
                                    var occupencycount = 0;
                                    // for (var k = 0; k < property_ids[j].propertyRooms.length; k++) {
                                    let column = "room_type_id"
                                    var roomtypeids = await RoomsModel.distinct(column, {
                                        property_id: properties_list[i].property_id,
                                        is_private: is_private
                                    })
                                    for (var j = 0; j < roomtypeids.length; j++) {
                                        var roomsintypeid = await RoomsModel.find({
                                            property_id: properties_list[i].property_id,
                                            room_type_id: roomtypeids[j],
                                            is_private: is_private
                                        })
                                        totunits = totunits + roomsintypeid.length
                                        var onwords = await RoomsModel.aggregate([
                                            {
                                                $match: {
                                                    $and: [
                                                        {
                                                            property_id: properties_list[i].property_id,
                                                            room_type_id: roomtypeids[j],
                                                            is_private: is_private
                                                        }
                                                    ]
                                                }
                                            },
                                            {
                                                $group:
                                                {
                                                    _id: "$room_type_id",
                                                    onwords: { $min: "$rent" }
                                                }
                                            }
                                        ]);
                                        if (onwords.length > 0) {
                                            minrent_list.push(onwords[0].onwords);
                                        }
                                    }
                                    // }
                                    data.push({
                                        property_id: properties_list[i].property_id,
                                        property_name: properties_list[i].property_name,
                                        property_address: properties_list[i].address,
                                        property_lat_long: properties_list[i].lat_long,
                                        property_images: properties_list[i].property_images,
                                        property_amenities: properties_list[i].amenities.length,
                                        total_units: totunits,
                                        is_private: is_private,
                                        occupency: occupencycount,
                                        price_starts_from: Math.min(...minrent_list)
                                    })
                                    // }
                                    // }
                                }
                                resultobj.notavailable = data
                                res.status(200).json(resultobj)
                            }
                            else {
                                res.status(400).json({
                                    status_code: 400,
                                    message: "Sorry no properties found for this filters"
                                })
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                            result = {
                                status_code: 401,
                                data: error
                            }
                        });
                }
                else {
                    res.status(400).json({
                        status_code: 200,
                        message: "End date should be Greater are equal to start date"
                    })
                }
            }
            else {
                res.status(400).json({
                    status_code: 200,
                    message: "Start date should be Greater are equal to Current date"
                })
            }
        }
    }
}