const router = require('express').Router();
const axios = require('axios');
var FormData = require('form-data');
var fs = require('fs');
const Joi = require('joi');
var request = require('request');
const formidable = require('formidable');
const CBAccess_Token = require('../Model/CBAccess_Token')

module.exports = {
  UploadKyc: async (req, res) => {
    const token = await CBAccess_Token.find({}, {})
    const form = formidable({ multiples: true })
    form.parse(req, async (err, fields, files) => {
      var data = new FormData();
      data.append('file', fs.createReadStream(files.file.path));

      var config = {
        method: 'post',
        url: 'https://ind-docs.hyperverge.co/v2.0/readKYC',
        headers: {
          'appid': '35fb98',
          'appkey': '398079e99403f49c3f15',
          'Content-Type': 'multipart/form-data',
          ...data.getHeaders()
        },
        data: data
      };

      axios(config)
        .then(async (response) => {
          // console.log(JSON.stringify(response.data));
          //console.log(req.body.Guest_id);
          // console.log(files.file.path,fields.Guest_id)
          var options = {
            'method': 'POST',
            'url': 'https://hotels.cloudbeds.com/api/v1.1/postGuestDocument',
            'headers': {
              'Authorization': 'Bearer ' + token[0].access_token,
              'Cookie': 'acessa_session=3f51e83ea882b6db781cb8e8fbf0f7cf308e5f14; identity=satish.y%40theretailinsights.com; remember_code=882e6aa81779194859288aa77f37df93d8fb25aa; HotelLng=en; _ga=GA1.3.2145971734.1609136285'
            },
            formData: {
              'guestID': fields.Guest_id,
              'file': {
                'value': fs.createReadStream(files.file.path),
                'options': {
                  'filename': files.file.name,//files.file.path,
                  'contentType': null
                }
              }
            }
          };
          await request(options, async (error, response) => {
            if (error) throw new Error(error);
            // console.log(response.body);
            if (JSON.parse(response.body).success) {
              res.send({
                'status_code': 200,
                'fileID': JSON.parse(response.body).data.fileID
              });
            }
            else {
              res.send({
                'status_code': 200,
                'message': JSON.parse(response.body)
              });
            }
          });
        })
        .catch(function (error) {
          res.status(500).json({
            status_code: 500,
            message: 'Document not correct'
          })
        });
    })

    // }

  }
}