const paymentListing = require('../Model/PaymentListing')

module.exports = {
    GetPaymentListing: async (req, res) => {
        res.status(200).json([
            {
                status_code : 200,
                message : "Request successfull",
                payments: [
                    {
                        paymentmethod: "RezorPay",
                        paymenturl: "http://olive-node.theretailinsights.co/api/RezorPay"
                    }
                ]
            }
        ])
    },

}