const express = require('express');
const RoomsModel = require('../Model/Rooms')
const PropertyModel = require('../Model/Properties')
const Joi = require('joi');
const axios = require('axios');
const CBAccess_Token = require('../Model/CBAccess_Token')
const moment = require('moment')
const Helper = require('../Helper/Helper')
const date = require('date-and-time');
const now = new Date();

module.exports = {
    PostRooms: async (req, res) => {
        new RoomsModel({
            property_id: 12345,
            room_type_id: 1234,
            room_rate_id: 12345,
            room_id: "1234-0",
            rent: 500,
            images: "path1,path2",
            occupency: "Private",
            facing: "East",
            floor: 1,
            room_size: "2000 sq ft",
            rating: 5
        }).save();
        res.status(200).send({ message: "Room Added" })
    },
    GetRoomsTypesList: async (req, res) => {
        const schema = Joi.object({
            property_id: Joi.number().required(),
            startdate: Joi.string().required(),
            enddate: Joi.string().required(),

            adults: Joi.number().integer().min(1).optional(),
            children: Joi.number().integer().min(0).optional(),

            minrate: Joi.number().integer().optional(),
            maxrate: Joi.number().integer().optional(),

            is_private: Joi.boolean().required(),
            pageNumber: Joi.number().required(),
            pageSize: Joi.number().required()
        });
        var result = {}
        const validationresult = schema.validate(req.body);
        if (validationresult.error) {
            let message = validationresult.error.details[0]['message'];
            result = {
                'message': message,
                'status_code': 400
            };
            res.status(400).json(result)
        }
        else {
            // var currentdate = new moment()
            var checkwithcurrentdate = await Helper.ComapreDates(date.format(now, 'DD/MM/YYYY'), new Date(req.body.startdate))
            if (checkwithcurrentdate) {
                var is_datesvalid = await Helper.ComapreDates(date.format(new Date(req.body.startdate), 'DD/MM/YYYY'), date.format(new Date(req.body.enddate), 'DD/MM/YYYY'))
                if (is_datesvalid) {
                    var data = [];
                    var property_id = req.body.property_id
                    var startdate = req.body.startdate
                    var enddate = req.body.enddate
                    var is_private = req.body.is_private

                    var adults = req.body.adults
                    var children = req.body.children
                    var minrate = req.body.minrate
                    var maxrate = req.body.maxrate

                    var pageNumber = req.body.pageNumber
                    var pageSize = req.body.pageSize
                    var url = "";
                    // console.log(minrate, maxrate, adults, children);
                    if (minrate > 0 && maxrate > 0 && adults > 0 && children >= 0) {
                        url = "https://hotels.cloudbeds.com/api/v1.1/getAvailableRoomTypes?propertyIDs=" + property_id + "&startDate=" + startdate + "&endDate=" + enddate + "&adults=" + adults + "&children=" + children + "&minRate=" + minrate + "&maxRate=" + maxrate + "&pageNumber=" + pageNumber + "&pageSize=" + pageSize
                    } else if (minrate == 0 && maxrate == 0 && adults > 0 && children >= 0) {
                        url = "https://hotels.cloudbeds.com/api/v1.1/getAvailableRoomTypes?propertyIDs=" + property_id + "&startDate=" + startdate + "&endDate=" + enddate + "&adults=" + adults + "&children=" + children + "&pageNumber=" + pageNumber + "&pageSize=" + pageSize
                    }
                    else if (minrate > 0 && maxrate > 0 && adults == 0 && children == 0) {
                        url = "https://hotels.cloudbeds.com/api/v1.1/getAvailableRoomTypes?propertyIDs=" + property_id + "&startDate=" + startdate + "&endDate=" + enddate + "&pageNumber=" + pageNumber + "&pageSize=" + pageSize
                    }
                    // console.log(url);
                    const token = await CBAccess_Token.find({}, {})
                    var config = {
                        method: 'get',
                        url: url,
                        headers: {
                            'Authorization': 'Bearer ' + token[0].access_token,
                            'Cookie': 'acessa_session=588519d7aeee34d0d9c1d5b543fb26959a7a44d4; csrf_accessa_cookie=72ead5e547d8554b08b98446f8adf658; identity=satish.y%40theretailinsights.com; remember_code=b2be63a890b441c63ed588750f74d2ab89ad701e; user_last_activity=1609134725; HotelDefLng=en; HotelLng=en; _ga=GA1.3.1439040734.1609133287'
                        }
                    };
                    await axios(config)
                        .then(async (response) => {
                            // console.log(JSON.stringify(response.data));
                            // res.status(200).json(response.data);
                            // process.exit();
                            // var constname = response.data.data[0].propertyRooms[0].roomTypeNameShort;
                            // console.log(JSON.stringify(response.data));
                            // console.log(response.data.length);
                            if (response.data.success) {
                                var roomtypes = response.data.data[0].propertyRooms
                                var property = await PropertyModel.find({ property_id: response.data.data[0].propertyID }, { _id: 0, __v: 0 });
                                for (var i = 0; i < roomtypes.length; i++) {
                                    var isprivate = await RoomsModel.find({ property_id: response.data.data[0].propertyID, room_type_id: roomtypes[i].roomTypeID, is_private: is_private }, { _id: 0, __v: 0 });
                                    // console.log(response.data.data[0].propertyID, roomtypes[i].roomTypeID, is_private)
                                    var onwords = await RoomsModel.aggregate([
                                        {
                                            $match: {
                                                $and: [
                                                    {
                                                        property_id: Number(response.data.data[0].propertyID),
                                                        room_type_id: roomtypes[i].roomTypeID,
                                                        is_private: is_private
                                                    }
                                                ]
                                            }
                                        },
                                        {
                                            $group:
                                            {
                                                _id: "$room_type_id",
                                                minrent: { $min: "$rent" }
                                            }
                                        }
                                    ]);
                                    // console.log(JSON.stringify(onwords1))
                                    if (isprivate.length > 0) {
                                        data.push({
                                            "property_id": property[0].property_id,
                                            "property_name": property[0].property_name,
                                            "property_desc": property[0].about,
                                            "property_address": property[0].address,
                                            "property_amenities": property[0].amenities,
                                            "lat_long": property[0].lat_long,
                                            "property_nearby": property[0].nearby,
                                            "roomTypeID": roomtypes[i].roomTypeID,
                                            "price_starts_from": onwords[0].minrent,
                                            "is_private": isprivate[0].is_private,
                                            "roomTypeName": roomtypes[i].roomTypeName,
                                            "roomsAvailable": roomtypes[i].roomsAvailable,
                                            "roomTypeDescription": roomtypes[i].roomTypeDescription,
                                            "roomTypePhotos": roomtypes[i].roomTypePhotos,
                                            "roomTypeNameShort": roomtypes[i].roomTypeNameShort
                                        })
                                    }
                                }
                                result = {
                                    status_code: 200,
                                    message: "Request successfull",
                                    data: data
                                }
                                res.status(200).json(result)
                            }
                            else {
                                result = {
                                    status_code: 200,
                                    message: response.data.message,
                                    data: []
                                }
                                res.status(200).json(result)
                            }
                        })
                        .catch(async (error) => {
                            // console.log(JSON.stringify(error));
                            result = {
                                status_code: 400,
                                data: error
                            }
                            res.status(400).json(result)
                        });
                }
                else {
                    res.status(400).json({
                        status_code: 200,
                        message: "End date should be Greater are equal to start date"
                    })
                }
            }
            else {
                res.status(400).json({
                    status_code: 200,
                    message: "Start date should be Greater are equal to Current date"
                })
            }
        }
        // console.log(result);
        // res.status(result.status_code).json(result)
    },
    GetRoomTypedetails: async (req, res) => {
        const schema = Joi.object({
            property_id: Joi.number().required(),
            roomtype_id: Joi.number().required(),
            roomtypeshortname: Joi.string().required(),
            startdate: Joi.string().required(),
            enddate: Joi.string().required(),
            is_private: Joi.boolean().required(),
            pageNumber: Joi.number().required(),
            pageSize: Joi.number().required(),
        });
        var result = {}
        const validationresult = schema.validate(req.body);
        if (validationresult.error) {
            result = {
                'message': validationresult.error.details,
                'status_code': 400
            }
            res.status(400).json(result);
        }
        else {
            var checkwithcurrentdate = await Helper.ComapreDates(date.format(now, 'DD/MM/YYYY'), new Date(req.body.startdate))
            if (checkwithcurrentdate) {
                var is_datesvalid = await Helper.ComapreDates(date.format(new Date(req.body.startdate), 'DD/MM/YYYY'), date.format(new Date(req.body.enddate), 'DD/MM/YYYY'))
                if (is_datesvalid) {
                    var property_id = req.body.property_id
                    var roomtype_id = req.body.roomtype_id
                    var roomtypeshortname = req.body.roomtypeshortname
                    var shortname = req.body.shortname
                    var startdate = req.body.startdate
                    var isprivate = req.body.is_private
                    var enddate = req.body.enddate
                    var pageNumber = req.body.pageNumber
                    var pageSize = req.body.pageSize
                    var data = []
                    const token = await CBAccess_Token.find({}, {})
                    var config = {
                        method: 'get',
                        url: "https://hotels.cloudbeds.com/api/v1.1/getRooms?propertyIDs=" + property_id + "&roomTypeID=" + roomtype_id + "&roomTypeNameShort=" + roomtypeshortname + "&startDate=" + startdate + "&endDate=" + enddate + "&pageNumber=" + pageNumber + "&pageSize=" + pageSize,
                        headers: {
                            'Authorization': 'Bearer ' + token[0].access_token,
                            'Cookie': 'acessa_session=588519d7aeee34d0d9c1d5b543fb26959a7a44d4; csrf_accessa_cookie=ca4e62faa91aded2d296c1ed19077a88; identity=satish.y%40theretailinsights.com; remember_code=b2be63a890b441c63ed588750f74d2ab89ad701e; user_last_activity=1609134725; HotelDefLng=en; HotelLng=en; _ga=GA1.3.1439040734.1609133287'
                        }
                    };
                    await axios(config)
                        .then(async (response) => {
                            var data = [];
                            // console.log(JSON.stringify(response.data));
                            if (response.data.success) {
                                if (response.data.data.length > 0) {
                                    var rooms = response.data.data[0].rooms;
                                    var propertydetails = await PropertyModel.find({ property_id: property_id }, { _id: 0, __v: 0 })
                                    var roomdetails = await RoomsModel.find({ property_id: property_id, room_type_id: roomtype_id, is_private: isprivate }, { _id: 0, __v: 0 })
                                    data.push({
                                        property_id: property_id,
                                        roomtype_id: roomdetails[0].room_type_id,
                                        room_rate_id: roomdetails[0].room_rate_id,
                                        //room_id: roomdetails[0].room_id,
                                        room_name: rooms[0].roomTypeName,
                                        roomtype_images: roomdetails[0].images,
                                        room_size: roomdetails[0].room_size,
                                        rent: roomdetails[0].rent,
                                        rentper_month: roomdetails[0].rent * 30,
                                        deposite: (roomdetails[0].rent * 30) * 2,
                                        property_amenities: propertydetails[0].amenities,
                                        room_amenities: roomdetails[0].amenities,
                                        rooms: []
                                    })
                                    var roomidslist = []
                                    //var roomslist = await RoomsModel.find({ property_id: property_id, room_type_id: roomtype_id, is_private: isprivate }, { _id: 0, __v: 0 })
                                    roomdetails.forEach(room => {
                                        roomidslist.push(room.room_id)
                                    });
                                    // console.log(roomidslist);process.exit()

                                    rooms.forEach(room => {
                                        var i = 0;
                                        roomdetails.forEach(dbroom => {
                                            //console.log(room.roomID, dbroom.room_id)
                                            if (room.roomID == dbroom.room_id) {
                                                data[0].rooms.push({
                                                    roomID: dbroom.room_id,
                                                    floor: dbroom.floor,
                                                    facing: dbroom.facing,
                                                })
                                            }
                                            i++;
                                        });
                                    });
                                    res.status(200).json(data);
                                }
                                else {
                                    res.status(400).json({
                                        status_code: 400,
                                        message: "Invalid Room type details"
                                    })
                                }
                            }
                            else {
                                res.status(400).json({
                                    status_code: 400,
                                    message: response.data.message
                                })
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                    // var roomtypedetails = RoomsModel.find({property_id : property_id,room_type_id : roomtype_id},{_id : 0,__v : 0})
                    // res.status(200).json(roomtypedetails)
                }
                else {
                    res.status(400).json({
                        status_code: 200,
                        message: "End date should be Greater are equal to start date"
                    })
                }
            }
            else {
                res.status(400).json({
                    status_code: 200,
                    message: "Start date should be Greater are equal to Current date"
                })
            }
        }
    }
}