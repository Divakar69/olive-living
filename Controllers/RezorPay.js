const router = require('express').Router();
var Razorpay = require("razorpay");
var Booking = require("../Model/Booking");
var axios = require('axios');

module.exports = {
    RezorPay: async function (req, res) {
        var amount = req.query.amount;
        var userid = req.query.userid;
        var response1 ={};
        var config = {
          method: 'get',
          url: 'http://olive.theretailinsights.co/api/v1/vendor/55',
          headers: { }
        };
        
        await axios(config)
        .then(function (result) {
            response1 = {
                data : result.data
            }
          //console.log(JSON.stringify(response.data));
        })
        .catch(function (error) {
          console.log(error);
        });
            let instance = new Razorpay({
            
            key_id: response1.data.data.vendor.client_id, // your `KEY_ID`
            key_secret: response1.data.data.vendor.client_secert // your `KEY_SECRET`
        });
        var params = {
            amount: amount,
            currency: "INR",
            receipt: "su001",
            payment_capture: "1"
        }
        var hostname = req.headers.host
        await instance.orders.create(params).then(async (data) => {
            // res.send(data.id)
            await new Booking({
                user_id: userid,
                payorderid: data.id
            }).save()
            res.render('layouts/main', {
                order_id: data.id,
                user_id: userid,
                amount: amount,
                hostname: hostname
            })
        });
    },
    verify: async (req, res) => {
        body = req.body.razorpay_order_id + "|" + req.body.razorpay_payment_id;
        var crypto = require("crypto");
        var expectedSignature = crypto.createHmac('sha256', 'S7xMd0C49caSKwTW6McDMpcJ')
            .update(body.toString())
            .digest('hex');
        // console.log("sig" + req.body.razorpay_signature);
        // console.log("sig" + expectedSignature);
        if (expectedSignature === req.body.razorpay_signature) {
            // res.status(200).send({
            //     status_code: 200,
            //     message: "Payment Successfull"
            // })
            // console.log('if')
            // res.status(200).send({
            //     status_code: 200,
            //     message: "Payment Successfull"
            // })
            // res.render('layouts/rpaySuccessReceipt', {
            //     status_code: 200,
            //     message: "Payment Successfull"
            // });
            // res.sendFile(__dirname + './Test.html')
            res.sendFile('C:/Users/divak/Embasy/Node/Test.html')
        }
        else {
            // res.status(400).send({
            //     status_code: 400,
            //     message: "Something went Wrong"
            // })
            // console.log('else')
            // res.render('layouts/rpaySuccessReceipt');
        }
    },
    Success: async (req, res) => {
        // var order_id = req.query.orderid
        // var order_idbody = req.body
        // console.log(order_id)
        var where = { _id: req.query.userid, payorderid: req.query.orderid }
        var newvalues = {
            $set: {
                paymentstatus: "success"
            }
        }
        await Booking.updateOne(
            where, newvalues
        )
        res.status(200).json({
            status_code: 200,
            message: "Success",
            order_id: req.query.orderid
        })
    },
    Failure: async (req, res) => {
        res.status(200).json({
            status_code: 500,
            message: "Payment fail",
            order_id: req.query.orderid
        })
    }
}