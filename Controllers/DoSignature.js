const Joi = require("joi")
const fs = require("fs")
const PDF = require('../Controllers/PDF')
const Booking = require('../Model/Booking')
var url = require('url')
const formidable = require('formidable');

module.exports = {
    SignRental: async (req, res) => {
        const form = formidable({ multiples: true })
        form.parse(req, async (err, fields, files) => {
            // console.log(fields)325312
            if (fields.reservationid != undefined && files.signature != undefined) {
                if (files.signature.size < 4000000) {
                    if (files.signature.type == 'image/png' || files.signature.type == 'image/jpg' || files.signature.type == 'image/jpeg') {
                        if (fields.reservationid) {
                            const reservationid = fields.reservationid
                            var params = {
                                pdf: "public/standard-residential-rental-lease-agreement.pdf",
                                signedrentalpath: "public/RentalAggrement/",
                                signaturepath: files.signature.path,//"public/files/" + req.file.filename,
                                imagetype: files.signature.type,//"public/files/" + req.file.filename,
                                reservationid: reservationid
                            }
                            let rentalaggrementpath = await PDF.AddImage(params)
                            if (rentalaggrementpath.success) {
                                res.status(200).json({
                                    status_code: 200,
                                    aggriment: "http://" + req.headers.host + "/" + rentalaggrementpath.path,//rentalaggrementpath.path,
                                    message: "Agreement signed successfully"
                                })
                            }
                            else {
                                res.status(400).json({
                                    status_code: 400,
                                    message: "Something went wrong please check image format"
                                })
                            }
                        }
                        else {
                            res.status(400).json({
                                status_code: 400,
                                message: "Reservation id required"
                            })
                        }
                    }
                }
                else {
                    res.status(400).json({
                        status_code: 400,
                        message: "Image size Should be Less than 4MB"
                    })
                }
            }
            else {
                res.status(400).json({
                    status_code: 400,
                    message: "Reservation ID or signature is missing"
                })
            }
        })
    }
}