const Luhn = require('luhn-js')
// const pdfsigner = require('pdf-signer')
// const fs = require('fs')
var Properties = require('../Model/Properties');
var Rooms = require('../Model/Rooms');

module.exports = {
    // Luhn: async (req, res) => {
    //     var luhnval = Luhn.generate('5559428101580477');
    //     var isvalid = Luhn.isValid(luhnval);
    //     console.log(luhnval, isvalid);
    // },
    // ResponseFromWebView: (req, res) => {
    //     res.status(200).json({
    //         status_code: 200,
    //         message: "Payment Successfull"
    //     })
    // },
    // CompareDates: async (req,res) => {
    //     var startdate = moment('02-01-2015')
    //     console.log(startdate)
    // }
    Postproperty: async (req, res) => {
        Properties.insertMany([{
            "property_images": [
                {
                    "thumb": "",
                    "image": ""
                }
            ],
            "lat_long": [
                "13.00679970,77.59709930"
            ],
            "address": [
                {
                    "propertyAddress1": "Building 6, 7th cross road, Ashwini layout",
                    "propertyAddress2": "",
                    "propertyCity": "Bengaluru",
                    "propertyState": "Karnataka",
                    "propertyZip": "560095",
                    "propertyCountry": "IN",
                    "propertyLatitude": "13.00679970",
                    "propertyLongitude": "77.59709930"
                }
            ],
            "amenities": [
                "Gaming area with table tennis",
                "carrom § Roof top terrace with yoga deck",
                "functional fitness ring",
                "and lounge area that host community events § Housekeeping- Includes dish washing",
                "in room cleaning",
                "and clearing out trash § Laundry area § Guest and Pet friendly § On call multi skilled technician § Wi-Fi with super-fast internet § Community area § Visitor parking § Parking § Water storage § Power back up § Waste disposal § Lift § Geysers § 24/7 CCTV surveillance"
            ],
            "nearby": [
                "Airport : 10 KM,Hospital:5 KM"
            ],
            "created_at": "2021/03/24 14:38:17",
            "property_id": 181961,
            "property_name": "Olive@Mohsham",
            "property_email": "olive@living.com",
            "property_phone": "6303732109",
            "about": "Contemporary lifestyle solutions fully equipped for people who enjoy hassle-free living. Olive Residences provides smart living solutions, optimized for rest, work, and play. We go beyond just offering a space to live in, but one where different cultures, opinions, and backgrounds can connect and be part of a vibrant community.<div>1 BHK residences with amenities that include housekeeping (dishwashing, in-room cleaning, clearing out trash, etc), 24 hours CCTV surveillance, security, WiFi, and on-call multi-skilled technician available.</div>"
        }, {
            "property_images": [
                {
                    "thumb": "",
                    "image": ""
                }
            ],
            "lat_long": [
                "13.00679970,77.59709930"
            ],
            "address": [
                {
                    "propertyAddress1": "Building 6, 7th cross road, Ashwini layout",
                    "propertyAddress2": "",
                    "propertyCity": "Bengaluru",
                    "propertyState": "Karnataka",
                    "propertyZip": "560095",
                    "propertyCountry": "IN",
                    "propertyLatitude": "13.00679970",
                    "propertyLongitude": "77.59709930"
                }
            ],
            "amenities": [
                "Gaming area with table tennis",
                "carrom § Roof top terrace with yoga deck",
                "functional fitness ring",
                "and lounge area that host community events § Housekeeping- Includes dish washing",
                "in room cleaning",
                "and clearing out trash § Laundry area § Guest and Pet friendly § On call multi skilled technician § Wi-Fi with super-fast internet § Community area § Visitor parking § Parking § Water storage § Power back up § Waste disposal § Lift § Geysers § 24/7 CCTV surveillance"
            ],
            "nearby": [
                "Airport : 10 KM,Hospital:5 KM"
            ],
            "created_at": "2021/03/24 14:38:17",
            "property_id": 181961,
            "property_name": "Olive@Mohsham",
            "property_email": "olive@living1.com",
            "property_phone": "9491500221",
            "about": "Contemporary lifestyle solutions fully equipped for people who enjoy hassle-free living. Olive Residences provides smart living solutions, optimized for rest, work, and play. We go beyond just offering a space to live in, but one where different cultures, opinions, and backgrounds can connect and be part of a vibrant community.<div>1 BHK residences with amenities that include housekeeping (dishwashing, in-room cleaning, clearing out trash, etc), 24 hours CCTV surveillance, security, WiFi, and on-call multi-skilled technician available.</div>"
        }])
    },
    PostRooms: async (req, res) => {
        Rooms.insertMany([{
            "images": [
                "https://h-img2.cloudbeds.com/uploads/181961/oliv1_logo~~5fbfd7355dc5c.png",
                "https://h-img1.cloudbeds.com/uploads/181961/dinning_~~5fbfd5d0b7a41.jpeg",
                "https://h-img2.cloudbeds.com/uploads/181961/sofa~~5fbfd5dc1ba79.jpg",
                "https://h-img1.cloudbeds.com/uploads/181961/bed1~~5fbfd6604726b.png"
            ],
            "checkin_date": "",
            "checkout_date": "",
            "amenities": [
                "Cable television",
                "Wireless internet (WiFi)"
            ],
            "created_at": "2021/03/30 09:51:31",
            "property_id": 181961,
            "room_type_id": 299801,
            "room_type_name": "1 BHK - Garden facing - Closed Kitchen",
            "room_name": "001",
            "room_id": "299801-0",
            "rent": "500",
            "is_private": false,
            "facing": "East",
            "floor": 1,
            "room_size": "1000 sq ft",
            "max_guest": "2",
            "adultsIncluded": "1",
            "childrenIncluded": "0",
            "rommTypeDescription": "<span>Fully furnished, cozy 1 BHK room with a garden facing view and a closed kitchen.<br></span>Room Description : Carpet area - 550 sqft, ensuite bathroom&nbsp;<b>. </b>Includes Dinning area ,Functional kitchen ,Designated living area with sofa cum bed &amp; Balcony<span><br><br><u>Amenities Included :</u><br></span>42 inch smart tv<br>192 litres refrigerator<br>&nbsp;Free wifi<span><br><br></span><br>"
        }, {
            "images": [
                "https://h-img2.cloudbeds.com/uploads/181961/oliv1_logo~~5fbfd7355dc5c.png",
                "https://h-img1.cloudbeds.com/uploads/181961/dinning_~~5fbfd5d0b7a41.jpeg",
                "https://h-img2.cloudbeds.com/uploads/181961/sofa~~5fbfd5dc1ba79.jpg",
                "https://h-img1.cloudbeds.com/uploads/181961/bed1~~5fbfd6604726b.png"
            ],
            "checkin_date": "",
            "checkout_date": "",
            "amenities": [
                "Cable television",
                "Wireless internet (WiFi)"
            ],
            "created_at": "2021/03/30 09:51:31",
            "property_id": 181961,
            "room_type_id": 299801,
            "room_type_name": "1 BHK - Garden facing - Closed Kitchen",
            "room_name": "002",
            "room_id": "299801-1",
            "rent": "500",
            "is_private": false,
            "facing": "East",
            "floor": 2,
            "room_size": "1000 sq ft",
            "max_guest": "2",
            "adultsIncluded": "1",
            "childrenIncluded": "0",
            "rommTypeDescription": "<span>Fully furnished, cozy 1 BHK room with a garden facing view and a closed kitchen.<br></span>Room Description : Carpet area - 550 sqft, ensuite bathroom&nbsp;<b>. </b>Includes Dinning area ,Functional kitchen ,Designated living area with sofa cum bed &amp; Balcony<span><br><br><u>Amenities Included :</u><br></span>42 inch smart tv<br>192 litres refrigerator<br>&nbsp;Free wifi<span><br><br></span><br>"
        }, {
            "images": [
                "https://h-img2.cloudbeds.com/uploads/181961/oliv1_logo~~5fbfd7355dc5c.png",
                "https://h-img1.cloudbeds.com/uploads/181961/dinning_~~5fbfd5d0b7a41.jpeg",
                "https://h-img2.cloudbeds.com/uploads/181961/sofa~~5fbfd5dc1ba79.jpg",
                "https://h-img1.cloudbeds.com/uploads/181961/bed1~~5fbfd6604726b.png"
            ],
            "checkin_date": "",
            "checkout_date": "",
            "amenities": [
                "Cable television",
                "Wireless internet (WiFi)"
            ],
            "created_at": "2021/03/30 09:51:31",
            "property_id": 181961,
            "room_type_id": 299801,
            "room_type_name": "1 BHK - Garden facing - Closed Kitchen",
            "room_name": "003",
            "room_id": "299801-2",
            "rent": "500",
            "is_private": false,
            "facing": "East",
            "floor": 3,
            "room_size": "1000 sq ft",
            "max_guest": "2",
            "adultsIncluded": "1",
            "childrenIncluded": "0",
            "rommTypeDescription": "<span>Fully furnished, cozy 1 BHK room with a garden facing view and a closed kitchen.<br></span>Room Description : Carpet area - 550 sqft, ensuite bathroom&nbsp;<b>. </b>Includes Dinning area ,Functional kitchen ,Designated living area with sofa cum bed &amp; Balcony<span><br><br><u>Amenities Included :</u><br></span>42 inch smart tv<br>192 litres refrigerator<br>&nbsp;Free wifi<span><br><br></span><br>"
        }])
    }
}