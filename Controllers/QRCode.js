const fs = require('fs');
const QRCode = require('qrcode');
const User_master = require('../Model/User_master');
const Helper = require('../Helper/Helper');
const crypto = require('crypto'); // For Encrypting and decrypting

module.exports = {
    QRCodeGen: async (req, res) => {
        var user_id = req.headers['userid']
        var userdetails = await User_master.find({ user_id: user_id }, {})
        var mobile = userdetails[0].mobile//await Helper.DeCrypt(userdetails[0].mobile)
        QRCode.toDataURL(mobile, { errorCorrectionLevel: 'H' }, function (err, base64) {
            if (err)
                res.status(400).json({
                    status_code: 400,
                    data: err
                })
            res.status(200).json({
                status_code: 200,
                data: base64
            })
        })
    }
}
