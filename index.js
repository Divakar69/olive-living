var express = require("express");
var app = express();
var csv = require("csv-parser");
var fs = require("fs");
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const ApiRoute = require('./Routes/api');
var expresshandlebars = require('express-handlebars')
// var requirejs = require('requirejs')
// var shell = require('shelljs')
// var cron = require('node-cron');
// var axios = require('axios');

dotenv.config();
app.use(express.json());
// app.use(requirejs);
app.use(express.static('public'))
app.listen(process.env.PORT, (err) => {
    if (err) console.log(err)
    console.log("I'm Listening from " + process.env.PORT)
})
app.use("/public", express.static(__dirname + '/public'));
// mongoose.connect(process.env.DB_CONNECT, { useNewUrlParser: true, useUnifiedTopology: true }, () => console.log('connected to db')
// );
mongoose.connect('mongodb://localhost/Embassy', { useNewUrlParser: true, useUnifiedTopology: true }, (err, info) => {
    if (err) console.log(err)
    else console.log('connected to db')
});

app.use('/api/', ApiRoute);

app.engine('handlebars', expresshandlebars({
    layoutsDir: `${__dirname}/views/layouts`
}))
app.set('view engine', 'handlebars');

// cron.schedule("1 * * * * *", async () => {// CB token Refresh
//     console.log("CB Scheduler Running...")
    // // "./Auth/CBToken.js"
    // // if (shell.exec("node ./Auth/CBToken.js").code !== 0) { //node CBtoken.js
    // //     console.log("Something Went wrong")
    // // }
    // // else {
    // //     console.log("CB Token refreshed successfully")
    // // }
    // var config = {
    //     method: 'get',
    //     url: 'http://olive-node.theretailinsights.co/api/RefreshToken',//http://olive-node.theretailinsights.co //localhost:9000
    //     headers: {}
    // };
    // await axios(config)
    //     .then(function (response) {
    //         console.log("token refresh success")
    //         // console.log(JSON.stringify(response.data));
    //     })
    //     .catch(function (error) {
    //         console.log("token refresh failed")
    //         console.log(error);
    //     });
// })
// cron.schedule("2 * * * * *", async () => {
//     console.log("Master Scheduler Running...")
    // //"./Controllers/Cron.js"
    // // if (shell.exec("node ./Controllers/Cron.js").code !== 0) { //node CBtoken.js
    // //     console.log("Something Went wrong")
    // // }
    // // else {
    // //     console.log("Masters Updated")
    // // }
    // var config = {
    //     method: 'get',
    //     url: 'http://olive-node.theretailinsights.co/api/UpdateMasters',//http://olive-node.theretailinsights.co //localhost:9000
    //     headers: {}
    // };

    // await axios(config)
    //     .then(function (response) {
    //         console.log('Masters Updated')
    //         // console.log(JSON.stringify(response.data));
    //     })
    //     .catch(function (error) {
    //         console.log('Masters Update failed')
    //         // console.log(error);
    //     });
// })