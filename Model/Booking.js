const mongoose = require('mongoose')
const date = require('date-and-time');
const now = new Date();

const Bookings = new mongoose.Schema(
    {
        user_id: {
            type: String,
            required: true,
        },
        property_id: {
            type: Number,
        },
        reservation_id: {
            type: Number,
        },
        payorderid: {
            type: String,
        },
        paymentstatus: {
            type: String,
        },
        cbtranid: {
            type: String,
        },
        cbtranstatus: {
            type: String,
        },
        cbbookingstatus: {
            type: String,
        },
        rentalaggrement: {
            type: String,
        },
        created_at: {
            type: String,
            require: true,
            default: date.format(now, 'YYYY/MM/DD HH:mm:ss')
        }
    }
)
module.exports = mongoose.model('Bookings', Bookings)