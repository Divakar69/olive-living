const mongoose = require('mongoose');

const usersettingsSchema = new mongoose.Schema({
    user_id: {
        type: String,
        required: true,
    },
    display_mode: {
        type: String,
        default: "Light"
    },
    notifications: {
        type: String,
        default: true
    },
    location: {
        type: String,
        default: true
    },
    country: {
        type: String,
        default: "India"
    },
    language: {
        type: String,
        default: "En"
    },
    timezone: {
        type: String,
        default: "GMT+5:30"
    },
    date: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('user_settings', usersettingsSchema);