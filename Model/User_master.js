const mongoose = require('mongoose');

const user_master = new mongoose.Schema({
    user_id: {
        type: String,
    },
    token: {
        type: String,
        default: ""
    },
    name: {
        type: String,
        default: ""
    },
    mobile: {
        type: String,
        required: true
    },
    email: {
        type: String,
        default: ""
    },
    password: {
        type: String,
        default: ""
    },
    DOB: {
        type: String,
        default: ""
    },
    profession: {
        type: String,
        default: ""
    },
    hobbies: {
        type: String,
        default: ""
    },
    passionate_about: {
        type: String,
        default: ""
    },
    sports: {
        type: String,
        default: ""
    },
    status: {
        type: String,
        default: ""
    },
    otp: {
        type: String,
        default: ""
    },
    date: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('User_master', user_master);
