const mongoose = require('mongoose');
const date = require('date-and-time');
const now = new Date();

const Rooms = new mongoose.Schema(
    {
        property_id: {
            type: Number,
            required: true,
        },
        room_type_id: {
            type: Number,
            required: true,
        },
        room_type_name: {
            type: String,
            required: true
        },
        room_name: {
            type: String,
            required: true
        },
        room_id: {
            type: String,
            required: true,
        },
        rent: {
            type: String,
            required: true,
        },
        images: {
            type: Array,
            required: true,
            default : []
        },
        facing: {
            type: String,
            required: true,
        },
        floor: {
            type: Number,
            required: true,
        },
        room_size: {
            type: String,
            required: true,
        },
        rating: {
            type: String
        },
        checkin_date: {
            type: String,
            default: ""
        },
        checkout_date: {
            type: String,
            default: ""
        },
        is_private: {
            type: Boolean,
            default: ""
        },
        amenities: {
            type: Array,
            default: []
        },
        max_guest: {
            type: String,
        },
        adultsIncluded: {
            type: String,
        },
        childrenIncluded: {
            type: String,
        },
        rommTypeDescription: {
            type: String,
        },
        rommDescription: {
            type: String,
        },
        created_at: {
            type: String,
            require: true,
            default: date.format(now, 'YYYY/MM/DD HH:mm:ss')
        }
    }
)
module.exports = mongoose.model('Rooms_master', Rooms)