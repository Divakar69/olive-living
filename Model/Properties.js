const mongoose = require('mongoose')
const date = require('date-and-time');
const now = new Date();

const Properties = new mongoose.Schema(
    {
        property_id: {
            type: Number,
            required: true,
        },
        property_name: {
            type: String,
            required: true,
        },
        property_email: {
            type: String,
            required: true,
        },
        property_phone: {
            type: String,
            required: true,
        },
        property_images: {
            type: Array,
            required: true,
        },
        lat_long: {
            type: Array,
            required: true,
        },
        address: {
            type: Array,
            required: true,
        },
        amenities: {
            type: Array,
            required: true
        },
        nearby: {
            type: Array,
            required: true
        },
        about: {
            type: String,
            required: true
        },
        created_at: {
            type: String,
            require: true,
            default: date.format(now, 'YYYY/MM/DD HH:mm:ss')
        }
    }
)
module.exports = mongoose.model('Property_master', Properties)