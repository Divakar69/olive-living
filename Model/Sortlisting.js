const mongoose = require('mongoose');

const sortlistingSchema = new mongoose.Schema({
    user_id: {
        type: String,
        required: true,
        min: 6,
        max: 255
    },
    property_id: {
        type: String,
        required: true,
        min: 6,
        max: 255
    },
    room_id: {
        type: String,
        required: true,
    },
    room_type_id: {
        type: String,
        required: true,
    },
    shortlist_id: {
        type: String,
        required: true
    },
    inserted_at: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Sortlisting', sortlistingSchema);
