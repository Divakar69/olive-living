const mongoose = require('mongoose');

const schedulevisitSchema = new mongoose.Schema({
    user_id: {
        type: String,
        required: true,
        min: 6,
        max: 255
    },
    visit_id: {
        type: String,
        required: true,
    },
    property_id: {
        type: String,
        required: true,
        min: 6,
        max: 255
    },
    room_id: {
        type: String,
        required: true,
        min: 6,
        max: 255
    },
    room_type_id: {
        type: String,
        required: true,
        min: 6,
        max: 255
    },
    date: {
        type: String,
        required: true,
    },
    time: {
        type: String,
        required: true,
        max: 255,
        min: 6
    },
    inserted_at: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Schedulevisit', schedulevisitSchema);
