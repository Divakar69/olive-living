const mongoose = require('mongoose')
const date = require('date-and-time');
const now = new Date();

const PaymentListing = new mongoose.Schema(
    {
        paymenturl: {
            type: String,
        },
        paymentname: {
            type: String
        },
        paymentcredentials: {
            type: Array
        },
        created_at: {
            type: String,
            require: true,
            default: date.format(now, 'YYYY/MM/DD HH:mm:ss')
        }
    }
)
module.exports = mongoose.model('PaymentListing', PaymentListing)