const mongoose = require('mongoose')
const date = require('date-and-time');
const now = new Date();

const cb_access_token = new mongoose.Schema(
    {
        access_token: {
            type: String,
            required: true,
        },
        refresh_token: {
            type: String,
            required: true
        },
        updated_at: {
            type: String,
            default: date.format(now, 'YYYY/MM/DD HH:mm:ss')
        },
        created_at: {
            type: String,
            default: date.format(now, 'YYYY/MM/DD HH:mm:ss')
        }
    }
)
module.exports = mongoose.model('cb_access_token', cb_access_token)