const express = require('express');
const jwt = require('jsonwebtoken');
const cryptr = require('cryptr');
const cryptcode = new cryptr('!@#$%^&*()')

var refreshtokens = [];

module.exports = {
    auth: async (req, res, next) => {
        if (!(typeof req.headers['authorization'] == undefined || req.headers['authorization'] == null)) {
            let token = req.headers['authorization'];
            token = token.split(' ')[1]
            jwt.verify(token, "access", (err, client) => {
                if (!err) {
                    // console.log(client)
                    req.client = client;
                    next();
                }
                else {
                    return res.status(400).send({ "message": "Access Denied" })
                }
            })
        }
        else {
            res.status(400).send({
                message: "Access token missing"
            })
        }
    },
    AccessToken: async (req, res) => {
        const client = req.body
        // console.log(client);
        if (!client) {
            return res.status(404).json({ message: "Body Empty" })
        }
        const client_id = req.body.client_id
        const client_secret = req.body.client_secret
        if (client_id == process.env.Client_id && client_secret == process.env.Client_secret) {
            let accessToken = jwt.sign(client, "access", { expiresIn: process.env.AccessTokenExpiryIn })
            let refreshToken = jwt.sign(client, "refresh", { expiresIn: process.env.RefreshTokenExpiryIn })
            refreshtokens.push(refreshToken);
            return res.status(200).json({
                accessToken,
                refreshToken
            })
        }
        else {
            return res.status(404).json({ message: "Invalid client details" })
        }
    },
    Protected: async (req, res) => {
        res.status(200).send("Inside protected route");
    },
    accesstokenWithRefreshtoken: async (req, res) => {
        const refreshtoken = req.body.token;
        // console.log(refreshtoken);
        if (refreshtoken == "" || !refreshtokens.includes(refreshtoken)) {
            res.status(400).send({ "message": "Invalid refresh token" })
        }
        jwt.verify(refreshtoken, "refresh", (err, client) => {
            if (!err) {
                var accesstoken = jwt.sign({ client: client.clientid }, "access", { expiresIn: process.env.AccessTokenExpiryIn })
                res.status(200).send({ accesstoken });
            }
            else {
                res.status(400).send({ "message": "Refresh token Expired" })
            }
        })
    }
}