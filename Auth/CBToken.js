const Helper = require('../Helper/Helper')
const CBAccess_Token = require('../Model/CBAccess_Token')
const FormData = require('form-data')
const axios = require('axios')
const date = require('date-and-time');
const now = new Date();

// function test(params) {
//     console.log("Test")
// }
// test()

async function refreshtoken() {
    const data = new FormData()
    const token = await CBAccess_Token.find({}, {});
    // console.log(token[0].refresh_token)
    data.append('grant_type', 'refresh_token');
    data.append('client_id', 'live1_181961_7CnLfN3FKtErsARHvucMbZlD');
    data.append('client_secret', 'EIN0mprCWv6zjGwRPkKluXhfQqSy82aB');
    data.append('refresh_token', token[0].refresh_token);
    var result = ""
    var config = {
        method: 'post',
        url: 'https://hotels.cloudbeds.com/api/v1.1/access_token',
        headers: {
            'Cookie': 'acessa_session=588519d7aeee34d0d9c1d5b543fb26959a7a44d4; identity=satish.y%40theretailinsights.com; remember_code=b2be63a890b441c63ed588750f74d2ab89ad701e; user_last_activity=1609134725; HotelDefLng=en; HotelLng=en; _ga=GA1.3.1439040734.1609133287',
            ...data.getHeaders()
        },
        data: data
    };

    await axios(config)
        .then(async (response) => {
            console.log(JSON.stringify(response.data));
            await CBAccess_Token.updateOne({ access_token: token[0].access_token }, {
                $set: {
                    access_token: response.data.access_token,
                    refresh_token: response.data.refresh_token,
                    updated_at: date.format(now, 'YYYY/MM/DD HH:mm:ss')
                }
            })
            result = true
            // return true
        })
        .catch(function (error) {
            console.log(error);
            result = false
            // return false
        });
    return result
}

module.exports = {
    checktokenandupdate: async (req, res, next) => {
        const data = new FormData()
        const token = await CBAccess_Token.find({}, {})
        let access_token = 'Bearer ' + token[0].access_token
        // console.log(access_token)
        var config = {
            method: 'post',
            url: 'https://hotels.cloudbeds.com/api/v1.1/access_token_check',
            headers: {
                'Authorization': "Bearer aeyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiJsaXZlMV8xODE5NjFfN0NuTGZOM0ZLdEVyc0FSSHZ1Y01iWmxEIiwianRpIjoiODIzZDczZWExY2YyNGJhZjc5MTM1NTVjNDIzMjdlNmZmYmM5YWYzMiIsImlhdCI6MTYxODQ2MzU4MSwibmJmIjoxNjE4NDYzNTgxLCJleHAiOjE2MTg0NjcxODEsInN1YiI6IjI4MjAwOCIsInNjb3BlcyI6W10sInR5cGUiOiJwcm9wZXJ0eSJ9.1ndTgdjX6krTI5TC9kVEdE-IOQFDDE0_662iRqDl7P_k0jqUy5LC5G4nNUhl9In0nsrtuhTLycNqt-hp4l0Tnuhw5tbQP92fk0wMc_KCW4SyLeZ311jSLB8wq9xspbMQa7TbCceZL6avlUkYkSGR2NdGCTwfT3v136Tzseh9mtGiNLL7ccBWOU_PuDuFohXyDClc6YSER9q02tY06BzN6xvqS2gVvD0-rW62oB1LGpEIxvb_jQb4H9jWeNLh-JKYjoza_LNxxN6YnlzaWGXd0_qswMKWqcimzgYSNkZgl1ZpOBHpnLxh_CQmlJwRgxJkx2BHZMGN4Med1tvkndvNrA",
                'Cookie': 'acessa_session=588519d7aeee34d0d9c1d5b543fb26959a7a44d4; identity=satish.y%40theretailinsights.com; remember_code=b2be63a890b441c63ed588750f74d2ab89ad701e; user_last_activity=1609134725; HotelDefLng=en; HotelLng=en; _ga=GA1.3.1439040734.1609133287',
                ...data.getHeaders()
            },
            data: data
        };

        await axios(config)
            .then(async (response) => {
                // console.log(JSON.stringify(response.data));
                var is_valid = response.data?.success || false
                if (is_valid) {
                    console.log("Token Till valid")
                    next()
                } else {
                    // console.log("else")
                    await refreshtoken()
                    console.log("Token Refreshed")
                    next()
                }
                // return is_valid
            })
            .catch(async (error) => {
                // console.log("catch")
                // console.log(error);
                let is_updated = await refreshtoken()
                if (is_updated)
                    next()
                else
                    console.log(error)
            });
    }
}

// module.exports = {
//     Check_Access_Token: async (req, res, next) => {
//         const token = await CBAccess_Token.find({}, {})
//         var config = {
//             method: 'post',
//             url: 'https://hotels.cloudbeds.com/api/v1.1/access_token_check',
//             headers: {
//                 'Authorization': `Bearer ${token[0].refresh_token}`,
//                 'Cookie': 'acessa_session=588519d7aeee34d0d9c1d5b543fb26959a7a44d4; csrf_accessa_cookie=692db824964adf3796152c1e5548781b; identity=satish.y%40theretailinsights.com; remember_code=b2be63a890b441c63ed588750f74d2ab89ad701e; user_last_activity=1609134725; AWSALB=clrXJ538gvJpAO/yyC81KjG31c7q4g7IDgP43BDZxfE/AeMUkSKUxTt4rVGvNGG9bC+yG2k7MHHi3aj0TdTvw/dftIQqxIl+MC4E9/kEBo59CuEeZiQefW3KSBcz; AWSALBCORS=clrXJ538gvJpAO/yyC81KjG31c7q4g7IDgP43BDZxfE/AeMUkSKUxTt4rVGvNGG9bC+yG2k7MHHi3aj0TdTvw/dftIQqxIl+MC4E9/kEBo59CuEeZiQefW3KSBcz; HotelDefLng=en; HotelLng=en; _ga=GA1.3.1439040734.1609133287',
//                 ...data.getHeaders()
//             },
//             data: data
//         };

//         await axios(config)
//             .then(async (response) => {
//                 // console.log(JSON.stringify(response.data));
//                 var is_valid = response.data?.success || false
//                 if (is_valid) {
//                     console.log("Token Till valid")
//                     next()
//                 } else {
//                     await RefreshToken()
//                     console.log("Token Refreshed")
//                     next()
//                 }
//             })
//         // .catch(function (error) {
//         //     console.log(error);
//         // });

//     },
//     RefreshToken: async (req, res) => {
//         const token = await CBAccess_Token.find({}, {})
//         // console.log(token)
//         // const isvalid = await Check_Token(token)
//         // var result = {}
//         // if (isvalid.success) {
//         data.append('grant_type', 'refresh_token');
//         data.append('client_id', 'live1_181961_7CnLfN3FKtErsARHvucMbZlD');
//         data.append('client_secret', 'EIN0mprCWv6zjGwRPkKluXhfQqSy82aB');
//         data.append('refresh_token', `${token[0].refresh_token}`);

//         var config = {
//             method: 'post',
//             url: 'https://hotels.cloudbeds.com/api/v1.1/access_token',
//             headers: {
//                 'Cookie': 'acessa_session=588519d7aeee34d0d9c1d5b543fb26959a7a44d4; identity=satish.y%40theretailinsights.com; remember_code=b2be63a890b441c63ed588750f74d2ab89ad701e; user_last_activity=1609134725; AWSALB=clrXJ538gvJpAO/yyC81KjG31c7q4g7IDgP43BDZxfE/AeMUkSKUxTt4rVGvNGG9bC+yG2k7MHHi3aj0TdTvw/dftIQqxIl+MC4E9/kEBo59CuEeZiQefW3KSBcz; AWSALBCORS=clrXJ538gvJpAO/yyC81KjG31c7q4g7IDgP43BDZxfE/AeMUkSKUxTt4rVGvNGG9bC+yG2k7MHHi3aj0TdTvw/dftIQqxIl+MC4E9/kEBo59CuEeZiQefW3KSBcz; HotelDefLng=en; HotelLng=en; _ga=GA1.3.1439040734.1609133287',
//                 ...data.getHeaders()
//             },
//             data: data
//         };

//         await axios(config)
//             .then(async (response) => {
//                 // console.log("success")
//                 // console.log(JSON.stringify(response.data));
//                 await CBAccess_Token.updateOne({ _id: token[0]._id }, { access_token: response.data.access_token, refresh_token: response.data.refresh_token })
//                 console.log("Token Refreshed successfully")
//                 res.status(200).json({
//                     status_code: 200,
//                     message: "Token Refreshed successfully"
//                 })
//             })
//             .catch(function (error) {
//                 console.log("failure")
//                 // console.log(error);
//                 res.status(400).json({
//                     status_code: 400,
//                     message: error
//                 })
//             });
//     }
// }