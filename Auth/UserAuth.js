const express = require('express');
const users = require('../Model/User_master')

module.exports = {
    Auth: async (req, res, next) => {
        if ((typeof req.headers['userid'] == undefined || req.headers['userid'] == null) || (typeof req.headers['token'] == undefined || req.headers['token'] == null)) {
            res.status(400).send({
                message: "User id or token is missing"
            })
        }
        else {
            const user_id = req.headers['userid']
            const token = req.headers['token']
            if (user_id.match(/^[0-9a-fA-F]{24}$/)) {
                var userdetails = await users.find({ _id: user_id, token: token }, {})
                if (userdetails.length > 0) {
                    next();
                }
                else {
                    res.status(400).json({
                        status_code: 400,
                        message: "Invalid user credentials"
                    })
                }
            }
            else
            {
                res.status(400).json({
                    status_code: 400,
                    message: "Invalid user credentials"
                })
            }
        }
    },
    PaymentAuth: async (req, res, next) => {
        if ((typeof req.query.userid == undefined || req.query.userid == null) || (typeof req.query.token == undefined || req.query.token == null)) {
            res.status(400).send({
                message: "User id or token is missing"
            })
        }
        else {
            const user_id = req.query.userid
            const token = req.query.token
            var userdetails = await users.find({ _id: user_id, token: token }, {})
            if (userdetails.length > 0) {
                next();
            }
            else {
                res.status(400).json({
                    status_code: 400,
                    message: "Invalid user credentials"
                })
            }
        }
    }
}