const Route = require('express').Router();
const Joi = require('joi');
var cron = require('node-cron')
const Kyc = require('../Controllers/Kyc')
const properties = require('../Controllers/Property')
const Rooms = require('../Controllers/Rooms')
const UserDetails = require('../Controllers/GetUserDetails')
const ScheduleVisit = require('../Controllers/ScheduleVisit')
// const GetScheduleVisit = require('../Controllers/ScheduleVisit')
// const DeleteScheduleVisit = require('../Controllers/ScheduleVisit')
const ProfileUpdate = require('../Controllers/ProfileUpdate')
const SetShortlist = require('../Controllers/Sortlisting')
const GetShortlist = require('../Controllers/Sortlisting')
const DeleteShortlist = require('../Controllers/Sortlisting')
const MyBookingsList = require('../Controllers/MyBookingsList')
const Booking = require('../Controllers/Booking')
const Test = require('../Controllers/Test')
const User = require('../Controllers/User')
const RezorPay = require('../Controllers/RezorPay')
const ExtendMyStay = require('../Controllers/ExtendMyStay')
const QRCode = require('../Controllers/QRCode')
const HomePage = require('../Controllers/HomePage')
const Calendar = require('../Controllers/Googlecalendar')

const PaymentLings = require('../Controllers/PaymentLings')
const DoSignature = require('../Controllers/DoSignature')
const OnDrive = require('../Controllers/OnDrive')
const Cron = require('../Controllers/Cron')
const CheckOut = require('../Controllers/CheckOut')
const AceestokenAuth = require('../Auth/Auth');
const CBToken = require('../Auth/CBToken');
// const CBToken = require('../Auth/CBToken');
const AceesAuth = AceestokenAuth.auth

const UserAuth = require('../Auth/UserAuth');
const Auth = UserAuth.Auth

Route.get('/GetAccessToken', AceestokenAuth.AccessToken)
Route.get('/RenewToken', AceestokenAuth.accesstokenWithRefreshtoken)

//------------------User--------------------\\
Route.post('/Login', AceesAuth, User.Login)
Route.post('/Logout', Auth, User.LogOut)
Route.post('/VerifyOtp', AceesAuth, User.VerifyOtp)
Route.post('/VerifyForgotPasswordOtp', AceesAuth, User.VerifyForgotPasswordOtp)
Route.post('/ResendOtp', AceesAuth, User.ResendOtp)
Route.put('/Forgotpassword', AceesAuth, User.Forgotpassword)
Route.put('/LoginWithPassword', AceesAuth, User.LoginWithPassword)
Route.put('/PutUserSettings', Auth, User.Set_User_settings)
Route.get('/getUserSettings', Auth, User.Get_User_Settings)
Route.get('/logout', Auth, User.Logout)
//-----------------End-----------------------\\

// ---------------CB Access------------------\\
// cron.schedule("50 * * * * *", async () => {// CB token Refresh
// Route.get('/RefreshToken', CBToken.RefreshToken)
// }),
// Route.get('/PostToken', CBAuth.PostToken)
// ---------------End CB Access------------------\\

//-----------------QR-------------------------\\
Route.get('/GenerateQRCode', QRCode.QRCodeGen)
//-----------------QR End-------------------------\\

//----------Rezor Pay ---------------------------//
Route.get('/RezorPay', UserAuth.PaymentAuth, RezorPay.RezorPay);
Route.post('/verify', RezorPay.verify);
Route.get('/RpaySuccess', RezorPay.Success);
Route.get('/RpayFailure', RezorPay.Failure);
// Route.get('/SuccessResponse', Test.ResponseFromWebView);
Route.get('/GetPaymentListing', Auth, PaymentLings.GetPaymentListing);
//----------End Rezor Pay ---------------------------//
Route.get('/Postproperty', Test.Postproperty);
Route.get('/PostRooms', Test.PostRooms);
// ---------------Cron--------------------//
// Route.get('/PropertyMaterUpdate', Cron.PropertyMaterUpdate)
// Route.get('/RoomsMasterUpdate', Cron.RoomsMasterUpdate)
// cron.schedule("* 23 * * * *", async () => {
Route.get('/UpdateMasters', CBToken.checktokenandupdate, Cron.UpdateMasters)
// }),
// ---------------End Cron--------------------//

// ------------------OneDrive-----------//
// Route.get('/OndriveAccesstoken', OnDrive.GetAccesstoken)
// Route.get('/OnDriveCallBack', OnDrive.OnDriveCallBack)
Route.get('/RoomsFileDownload', OnDrive.RoomsFileDownload)
Route.get('/PrpertiesFileDownload', OnDrive.PrpertiesFileDownload)
//-------------------EndOneDrive--------//
// Route.get('/GetProperties', getauth, properties.GetProperties)
Route.get('/GetProperties', Auth, CBToken.checktokenandupdate, properties.GetPropertyList)
Route.post('/PostProperty', Auth, CBToken.checktokenandupdate, properties.PostProperty)
Route.get('/PropertyDetails', Auth, CBToken.checktokenandupdate, properties.PropertyDetails)
Route.post('/PostRooms', Auth, CBToken.checktokenandupdate, Rooms.PostRooms)
Route.get('/GetRoomsTypesList', Auth, CBToken.checktokenandupdate, Rooms.GetRoomsTypesList)
Route.get('/GetRoomTypedetails', Auth, CBToken.checktokenandupdate, Rooms.GetRoomTypedetails)
Route.get('/GetRooms', Auth, CBToken.checktokenandupdate, Rooms.GetRoomsTypesList)
Route.get('/UserDetails', Auth, CBToken.checktokenandupdate, UserDetails.GetUserDetails)

Route.post('/SetScheduleVisit', Auth, CBToken.checktokenandupdate, ScheduleVisit.SetVisit)
Route.get('/GetScheduleVisit', Auth, CBToken.checktokenandupdate, ScheduleVisit.GetVisits)
Route.get('/GetAvailableSlots', Auth, CBToken.checktokenandupdate, ScheduleVisit.GetAvailableVisitSlots)
Route.post('/PostVisit', Auth, CBToken.checktokenandupdate, ScheduleVisit.Postvisit)
Route.delete('/DeleteScheduleVisit', Auth, CBToken.checktokenandupdate, ScheduleVisit.DeleteVisit)

Route.post('/ProfileUpdate', Auth, ProfileUpdate.ProfileUpdate)

Route.post('/SetShortlist', Auth, SetShortlist.ShortList)
Route.get('/GetShortlist', Auth, GetShortlist.GetShortLists)
Route.delete('/DeleteShortlist', Auth, DeleteShortlist.DeleteShortList)

//------------Bokking------------------------//
Route.get('/MyBookingsList', Auth, CBToken.checktokenandupdate, MyBookingsList.GetReservations)
Route.post('/PostBookins', Auth, CBToken.checktokenandupdate, MyBookingsList.PostBookins)
Route.post('/ExtendMyStay', Auth, CBToken.checktokenandupdate, ExtendMyStay.ExtendStay)
Route.post('/Booking', Auth, CBToken.checktokenandupdate, Booking.Booking)
Route.post('/Pay', Auth, Booking.Pay)
Route.put('/CancelBooking', Auth, CBToken.checktokenandupdate, Booking.CancelBooking)
Route.get('/GetInvoice', Auth, CBToken.checktokenandupdate, Booking.GetInvoice)
Route.get('/GetHomepage', Auth, CBToken.checktokenandupdate, HomePage.GetHome);
Route.get('/getcalendar', Calendar.Getcalendar);
Route.post('/kyc', Auth, Kyc.UploadKyc)
Route.put('/Checkout', Auth, CBToken.checktokenandupdate, CheckOut.Checkout);
//---------------End Booking--------------------//

Route.post('/SignRental', Auth, CBToken.checktokenandupdate, DoSignature.SignRental)

module.exports = Route;
