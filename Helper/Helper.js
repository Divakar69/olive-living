const router = require('express').Router();
const Joi = require('joi');
const Cryptr = require('cryptr'); // For Encrypting and decrypting
const cryptr = new Cryptr('Retail@123')
const bcrypt = require('bcryptjs');// For Generating Token
const axios = require('axios');
var csv = require("csv-parser");
const readXlsxFile = require('read-excel-file/node');
var fs = require("fs");
var nodemailer = require('nodemailer');
var moment = require('moment');
const oneDriveAPI = require('onedrive-api');


module.exports = {
    EnCrypt: function (value) {

        // var cryptvalue = cryptr.createCipher('aes-128-cbc', 'cryptrvalue');
        // var cryptedvalue = cryptvalue.update(value, 'utf8', 'hex')
        // cryptedvalue += cryptvalue.final('hex');
        var cryptedvalue = cryptr.encrypt(value)
        return cryptedvalue;
    },
    EnCrypt1: (value) => {
        const algorithm = 'aes-256-ctr';
        const ENCRYPTION_KEY = Buffer.from('FoCKvdLslUuB4y3EZlKate7XGottHski1LmyqJHvUhs=', 'base64'); // or generate sample key Buffer.from('FoCKvdLslUuB4y3EZlKate7XGottHski1LmyqJHvUhs=', 'base64');
        // console.log(ENCRYPTION_KEY)
        const IV_LENGTH = 16;
        let iv = cryptr.randomBytes(IV_LENGTH);
        let cipher = cryptr.createCipheriv(algorithm, Buffer.from(ENCRYPTION_KEY, 'hex'), iv);
        let encrypted = cipher.update(value);
        encrypted = Buffer.concat([encrypted, cipher.final()]);
        return iv.toString('hex') + ':' + encrypted.toString('hex');
    },
    // Usefull ------

    // const cryptr = require('cryptr');
    // const algorithm = 'aes-256-ctr';
    // const ENCRYPTION_KEY = 'Put_Your_Password_Here'; // or generate sample key Buffer.from('FoCKvdLslUuB4y3EZlKate7XGottHski1LmyqJHvUhs=', 'base64');
    // const IV_LENGTH = 16;
    // function encrypt(text) {
    //     let iv = cryptr.randomBytes(IV_LENGTH);
    //     let cipher = cryptr.createCipheriv(algorithm, Buffer.from(ENCRYPTION_KEY, 'hex'), iv);
    //     let encrypted = cipher.update(text);
    //     encrypted = Buffer.concat([encrypted, cipher.final()]);
    //     return iv.toString('hex') + ':' + encrypted.toString('hex');
    // }
    // function decrypt(text) {
    //     let textParts = text.split(':');
    //     let iv = Buffer.from(textParts.shift(), 'hex');
    //     let encryptedText = Buffer.from(textParts.join(':'), 'hex');
    //     let decipher = cryptr.createDecipheriv(algorithm, Buffer.from(ENCRYPTION_KEY, 'hex'), iv);
    //     let decrypted = decipher.update(encryptedText);
    //     decrypted = Buffer.concat([decrypted, decipher.final()]);
    //     return decrypted.toString();
    // }

    ////////////////////////
    DeCrypt: function (value) {
        // var decryptvalue = cryptr.createDecipher('aes-128-cbc', 'cryptrvalue');
        // var decryptedvalue = decryptvalue.update(value, 'hex', 'utf8')
        // decryptedvalue += decryptvalue.final('utf8');
        var decryptedvalue = cryptr.decrypt(value)
        return decryptedvalue;
    },
    DeCrypt1: async (value) => {
        const algorithm = 'aes-256-ctr';
        const ENCRYPTION_KEY = Buffer.from('FoCKvdLslUuB4y3EZlKate7XGottHski1LmyqJHvUhs=', 'base64'); // or generate sample key Buffer.from('FoCKvdLslUuB4y3EZlKate7XGottHski1LmyqJHvUhs=', 'base64');
        let textParts = value.split(':');
        let iv = Buffer.from(textParts.shift(), 'hex');
        let encryptedText = Buffer.from(textParts.join(':'), 'hex');
        let decipher = cryptr.createDecipheriv(algorithm, Buffer.from(ENCRYPTION_KEY, 'hex'), iv);
        let decrypted = decipher.update(encryptedText);
        decrypted = Buffer.concat([decrypted, decipher.final()]);
        return decrypted.toString();
    },
    GetToken: async function () {
        const salt = await bcrypt.genSalt(10);
        const token = await bcrypt.hash("Token" + Date.now(), salt);
        return token;
    },
    GetOTP: async function () {
        const otp = await Math.floor(Math.random() * (9999 - 1000)) + 1000;
        return otp
    },
    SMS: async (params) => {
        await axios.post('https://api.myvfirst.com/psms/servlet/psms.JsonEservice', {
            '@VER': '1.2',
            USER: {
                '@USERNAME': "embassyxmltrn",
                '@PASSWORD': 'embsy001',
                '@UNIXTIMESTAMP': ""
            },
            DLR: { '@URL': "" },
            SMS: [{
                '@UDH': "0",
                '@CODING': "1",

                '@TEXT': "5432 is the One  Time Password(OTP) for your login to Oliveliving. Please do not share with anyone.", "@PROPERTY": "0", "@ID": "1",
                ADDRESS: [{
                    '@FROM': "EMRESI",
                    '@TO': "916303732109",
                    '@SEQ': "1",
                    '@TAG': ""
                },
                ]
            }]
        })
            .then((ApiResponse) => {
                console.log("then")
                var response = {
                    'status_code': 200,
                    'message': "OTP send to"
                }
                return response;
                //console.log(response);
            })
            .catch((error) => {
                console.log("catch")
                var response = {
                    'status_code': 200,
                    'message': error
                }
                // console.log(response);
                return "response";
            });
    },
    Isset: async (variable) => {
        if (variable == undefined) {
            return false
        }
        else {
            return true
        }
    },
    CSVtoJson: async (params) => {
        // console.log(params.csvfilepath)
        fs.createReadStream(params.csvfilepath).pipe(csv()).on('data', (row) => {
            console.log(row)
        })
    },
    Email: async (params) => {
        let transport = nodemailer.createTransport({
            host: process.env.Emailhost,
            port: process.env.EmailPort,
            auth: {
                user: process.env.Emailuser,
                pass: process.env.Emailpass
            }
        });
        const message = {
            from: process.env.Emailuser, // Sender address
            to: params.email,         // List of recipients
            subject: params.subject,//'Error in ' + params.csvname + "CSV", // Subject line
            html: params.html//'<p>Hi &nbsp;' + "Divakar" + ',</p><p>Below is the error in &nbsp;' + params.csvname + "CSV" + '</p><br><p>' + params.errormsg + '</p></p><br><p>Please update the csv:&nbsp;</p><br><br><p>Thank you!</p><p>Developer</p>'
        };
        await transport.sendMail(message, (err, info) => {
            if (err) {
                console.log(err)
                return false
            } else {
                console.log(info)
                return true;
            }
        })
    },
    XLSXtoObjectOfArrays: async (params) => {
        //"./properties.xlsx"
        var objectofarray = {}
        var resultarray = []
        try {
            await readXlsxFile(params.xlspath).then((rows) => {
                // `rows` is an array of rows
                // each row being an array of cells.
                objectofarray = rows
            })
        }
        catch (error) {
            console.log(error)
        }
        var keys = objectofarray[0]
        var a = 0;
        objectofarray.forEach(row => {
            if (a != 0) {
                var i = 0;
                var rowvalues = {}
                for (var i = 0; i < row.length; i++) {
                    rowvalues[keys[i]] = row[i]
                }
                resultarray.push({
                    rowvalues
                })
            }
            a++
        });
        return resultarray
    },
    CallAPI_Axios: async (params) => {
        var result = {}
        var config = {
            method: params.method,//'get',
            url: params.url,//'https://hotels.cloudbeds.com/api/v1.1/getRooms',
            headers: {
                'Authorization': 'Bearer ' + params.access_token,
                'Cookie': 'acessa_session=588519d7aeee34d0d9c1d5b543fb26959a7a44d4; csrf_accessa_cookie=6b21539c2247902e563677588a1d9997; identity=satish.y%40theretailinsights.com; remember_code=b2be63a890b441c63ed588750f74d2ab89ad701e; user_last_activity=1609134725; AWSALB=LGm5rKKViphabml6MCSPJXWkMM0brauAT/S/AsFUcCgKKUOD/BpSWOl9Wb/aIihhRF0NDuwiODXO/2TgIMQrNvBBsHWQxF/MbyBH60HThqSNijRD7K0ZHbfxGlUC; AWSALBCORS=LGm5rKKViphabml6MCSPJXWkMM0brauAT/S/AsFUcCgKKUOD/BpSWOl9Wb/aIihhRF0NDuwiODXO/2TgIMQrNvBBsHWQxF/MbyBH60HThqSNijRD7K0ZHbfxGlUC; HotelDefLng=en; HotelLng=en; _ga=GA1.3.1439040734.1609133287'
            }
        };
        await axios(config)
            .then(async (response) => {
                // console.log(JSON.stringify(response.data));
                result = response.data
                // return response.data
            })
            .catch(async (error) => {
                // console.log(error);
                result = error
                // return error
            });
        return result
    },
    // CalAPi_AxiosPost: async (params) => {
    //     var config = {
    //         method: params.method,
    //         url: params.url,
    //         headers: {
    //             'Cookie': 'acessa_session=588519d7aeee34d0d9c1d5b543fb26959a7a44d4; identity=satish.y%40theretailinsights.com; remember_code=b2be63a890b441c63ed588750f74d2ab89ad701e; user_last_activity=1609134725; AWSALB=clrXJ538gvJpAO/yyC81KjG31c7q4g7IDgP43BDZxfE/AeMUkSKUxTt4rVGvNGG9bC+yG2k7MHHi3aj0TdTvw/dftIQqxIl+MC4E9/kEBo59CuEeZiQefW3KSBcz; AWSALBCORS=clrXJ538gvJpAO/yyC81KjG31c7q4g7IDgP43BDZxfE/AeMUkSKUxTt4rVGvNGG9bC+yG2k7MHHi3aj0TdTvw/dftIQqxIl+MC4E9/kEBo59CuEeZiQefW3KSBcz; HotelDefLng=en; HotelLng=en; _ga=GA1.3.1439040734.1609133287',
    //             ...data.getHeaders()
    //         },
    //         data: params.data
    //     };
    //     await axios(config)
    //         .then(function (response) {
    //             console.log(JSON.stringify(response.data));
    //         })
    //         .catch(function (error) {
    //             console.log(error);
    //         });
    // },
    OndriveFileDownload: async (params) => {
        // try {
        // const fileStream = await oneDriveAPI.items.download({
        //     accessToken: "Bearer " + params.OneDriveAccess_token,
        //     itemId: params.itemID//'2385EED47681663A!136'
        // });
        // const file = fs.createWriteStream(params.filename)//params.filename)//'properties.xlsx')
        // fileStream.pipe(file);
        const fileStream = await oneDriveAPI.items.download({
            accessToken: "Bearer " + process.env.OndriveToken,
            itemId: '2385EED47681663A!137'
        });
        const file = fs.createWriteStream('rooms.xlsx')
        fileStream.pipe(file);
        return true
        // }
        // catch (error) {
        //     console.log(error)
        //     return false
        // }
    },
    ComapreDates: async (date1, date2) => {
        var startdate = moment(date1, 'DD/MM/YYYY')
        var enddate = moment(date2, 'DD/MM/YYYY')
        // console.log(startdate > enddate)
        return (startdate <= enddate) ? true : false
    }
}
